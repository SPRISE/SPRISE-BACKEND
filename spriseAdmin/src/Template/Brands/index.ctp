<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-body">
                <div id="grid-layout-ul-li" class="box jplist">
                    <div class="jplist-ios-button"><i class="fa fa-sort"></i>jPList Actions</div>
                    <div class="jplist-panel box panel-top">
                        <button type="button" data-control-type="reset" data-control-name="reset" data-control-action="reset" class="jplist-reset-btn btn btn-default">Reset<i class="fa fa-share mls"></i></button>

                        <div class="text-filter-box">
                            <div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input data-path=".title" type="text" value="" placeholder="Filter by Title" data-control-type="textbox" data-control-name="title-filter" data-control-action="filter" class="form-control"/></div>
                        </div>
                        <div class="text-filter-box">
                            <div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input data-path=".desc" type="text" value="" placeholder="Filter by Description" data-control-type="textbox" data-control-name="desc-filter" data-control-action="filter" class="form-control"/></div>
                        </div>
                        <div data-control-type="views" data-control-name="views" data-control-action="views" data-default="jplist-grid-view" class="jplist-views">
                            <button type="button" data-type="jplist-list-view" class="jplist-view jplist-list-view btn btn-default"><i class="fa fa-th-list"></i></button>
                            <button type="button" data-type="jplist-grid-view" class="jplist-view jplist-grid-view btn btn-default"><i class="fa fa-th"></i></button>
                            <button type="button" data-type="jplist-thumbs-view" class="jplist-view jplist-thumbs-view btn btn-default"><i class="fa fa-reorder"></i></button>

                        </div>

                        <div data-control-type="pagination" data-control-name="paging" data-control-action="paging" class="jplist-pagination"></div>
                        <div class="jplist-reset-btn btn btn-default btn-info"><i class="fa fa-plus"></i>&nbsp;
                            <?= $this->Html->link(__('New Brand', true), array('action' => 'add'), array('class' => ['jplist-reset-btn', 'btn btn-default', 'btn-info'])); ?></a>&nbsp;
                        </div>
                    </div>
                    <ul class="box text-shadow ul-li-list"><!--<item>1</item>-->

                        <?php foreach ($brands as $brand): ?>
                            <li class="list-item" style="height: 330px;">
                                <div class="img" style="height: 220px;width: 220px;">
                                    <img src="<?= h($brand->company_icon) ?>" alt="" title="" 
                                         height="220px" width="220px"
                                         style="background-size: 100%;width:100%;height:100%;"/>
                                </div>
                                <!--<data></data>-->
                                <div class="block"><p class="date"><?= h($brand->create_at) ?></p>
                                    <?= $this->Html->link(__('Edit Brand'), ['action' => 'edit', $brand->id]) ?>
                                    <p class="title"><?= h($brand->company_name) ?></p>
    <!--                                        <p class="desc"><?= $brand->has('city') ? $this->Html->link($brand->city->title, ['controller' => 'Cities', 'action' => 'view', $brand->city->id]) : '' ?></p>
                                    <p class="like"><?= $brand->has('state') ? $this->Html->link($brand->state->title, ['controller' => 'States', 'action' => 'view', $brand->state->id]) : '' ?></p>-->
                                    <p class="desc"><?= $brand->city->title; ?></p>
                                    <p class="like"><?= $brand->state->title; ?></p>

                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <div class="box jplist-no-results text-shadow align-center"><p>No results found</p></div>
                    <div class="jplist-ios-button"><i class="fa fa-sort"></i>jPList Actions</div>
                    <div class="jplist-panel box panel-bottom">
                        <div data-control-type="drop-down" data-control-name="paging" data-control-action="paging" data-control-animate-to-top="true" class="jplist-drop-down form-control">
                            <ul class="dropdown-menu">
                                <li><span data-number="3"> 3 per page</span></li>
                                <li><span data-number="5"> 5 per page</span></li>
                                <li><span data-number="10" data-default="true"> 10 per page</span></li>
                                <li><span data-number="all"> view all</span></li>
                            </ul>
                        </div>
                        <div data-control-type="drop-down" data-control-name="sort" data-control-action="sort" data-control-animate-to-top="true" data-datetime-format="{month}/{day}/{year}" class="jplist-drop-down form-control">
                            <ul class="dropdown-menu">
                                <li><span data-path="default">Sort by</span></li>
                                <li><span data-path=".title" data-order="asc" data-type="text">Title A-Z</span></li>
                                <li><span data-path=".title" data-order="desc" data-type="text">Title Z-A</span></li>
                                <li><span data-path=".desc" data-order="asc" data-type="text">Description A-Z</span></li>
                                <li><span data-path=".desc" data-order="desc" data-type="text">Description Z-A</span></li>
                                <li><span data-path=".like" data-order="asc" data-type="number" data-default="true">Likes asc</span></li>
                                <li><span data-path=".like" data-order="desc" data-type="number">Likes desc</span></li>
                                <li><span data-path=".date" data-order="asc" data-type="datetime">Date asc</span></li>
                                <li><span data-path=".date" data-order="desc" data-type="datetime">Date desc</span></li>
                            </ul>
                        </div>
                        <div data-type="{start} - {end} of {all}" data-control-type="pagination-info" data-control-name="paging" data-control-action="paging" class="jplist-label btn btn-default"></div>
                        <div data-control-type="pagination" data-control-name="paging" data-control-action="paging" data-control-animate-to-top="true" class="jplist-pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--LOADING SCRIPTS FOR Grid Layout PAGE-->
<?= $this->Html->script('../vendors/jplist/html/js/vendor/modernizr.min.js', array('inline' => false)); ?> 
<?= $this->Html->script('../vendors/jplist/html/js/jplist.min.js', array('inline' => false)); ?> 
<?= $this->Html->script('jplist.js', array('inline' => false)); ?> 
