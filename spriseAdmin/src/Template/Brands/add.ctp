<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Brand $brand
 */
?>
<div id="tab-two-columns-horizontal" >
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-yellow">
                <div class="panel-body pan">
                    <?= $this->Form->create($brand, ['class' => 'form-horizontal', 'type' => 'file']); ?>
                    <div class="form-body pal"><h3>Brand</h3>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group"><label for="inputFirstName" class="col-md-3 control-label">Company Name <span class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <?= $this->Form->control('company_name', ['label' => false, 'Required' => 'Required', 'placeholder' => 'Company Name', 'class' => 'form-control']); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group"><label for="inputLastName" class="col-md-3 control-label">Street Name <span class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <div class="input-icon">
                                            <?= $this->Form->control('street_name', ['Required' => 'Required', 'label' => false, 'placeholder' => 'Street Name', 'class' => 'form-control']); ?></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group"><label for="inputLastName" class="col-md-3 control-label">About Company <span class='require'>*</span></label>

                                    <div class="col-md-9"><?= $this->Form->control('about_company', ['Required' => 'Required', 'label' => false, 'placeholder' => 'About Company', 'class' => 'form-control']); ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group"><label for="inputLastName" class="col-md-3 control-label">State <span class='require'>*</span></label>

                                    <div class="col-md-9">

                                        <?= $this->Form->control('state_id', ['Required' => 'Required', 'label' => false, 'options' => $states, 'empty' => true, 'class' => 'form-control']); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group"><label for="inputCity" class="col-md-3 control-label">City <span class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <?= $this->Form->control('city_id', ['Required' => 'Required', 'label' => false, 'options' => [], 'empty' => true, 'class' => 'form-control']); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group"><label for="inputFirstName" class="col-md-3 control-label">Company Logo <span class='require'>*</span></label>
                                    <div class="col-md-9">
                                        <?= $this->Form->control('company_icon', ['Required' => 'Required', 'type' => 'file', 'label' => false, 'placeholder' => 'Company Name', 'class' => ['form-control', 'btn']]); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group"><label for="selGender" class="col-md-3 control-label">Status <span class='require'>*</span></label>

                                    <div class="col-md-9"><?php
                                        $options_status = ['ACTIVE' => 'Active', 'DEACTIVE' => 'Inactive'];
                                        echo $this->Form->control('status', ['Required' => 'Required', 'label' => false, 'options' => $options_status, 'empty' => false, 'class' => 'form-control']);
                                        ?>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <h3>Contact Person</h3>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group"><label for="inputCity" class="col-md-3 control-label">Contact No <span class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <?= $this->Form->control('contact_no', ['onkeydown'=>'return FilterInput(event)', 'onpaste'=>'handlePaste(event)','Required' => 'Required', 'label' => false, 'class' => 'form-control', 'placeholder' => 'Phone Number']); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group"><label for="inputAddress2" class="col-md-3 control-label">E-mail<span class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <?= $this->Form->control('email', ['Required' => 'Required', 'label' => false, 'class' => 'form-control', 'placeholder' => 'email@yourcompany.com']); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group"><label for="inputStates" class="col-md-3 control-label">Password<span class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <?=
                                        $this->Form->control('password', ['Required' => 'Required', 'label' => false,
                                            'placeholder' => 'password',
                                            'type' => 'password',
                                            'class' => 'form-control']);
                                        ?></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group"><label for="selCountry" class="col-md-3 control-label">Gender<span class='require'>*</span></label>

                                    <div class="col-md-9"><?php
                                        $options_gender = ['Male' => 'Male', 'Female' => 'Female'];
                                        echo $this->Form->control('gender', ['Required' => 'Required', 'label' => false, 'options' => $options_gender, 'empty' => false, 'class' => 'form-control'], 'Male');
                                        ?></div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group"><label for="inputPostCode" class="col-md-3 control-label">Confirm Password<span class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <?=
                                        $this->Form->control('confirm_password', ['Required' => 'Required', 'label' => false,
                                            'placeholder' => 'Retype Password',
                                            'type' => 'password',
                                            'class' => 'form-control']);
                                        ?>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="form-actions text-right pal">
                        <?= $this->Form->button(__('Submit'), ['class' => ['btn', 'btn-primary']]); ?>
                        &nbsp;
                        <button type="button" class="btn btn-green">Cancel</button>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('app/app.js'); ?>    