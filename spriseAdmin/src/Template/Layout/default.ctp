<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head><title>Sprise | Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="images/icons/favicon.ico">
        <link rel="apple-touch-icon" href="images/icons/favicon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="images/icons/favicon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="images/icons/favicon-114x114.png">
        <!--Loading bootstrap css-->
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
        <?= $this->Html->css('../vendors/jquery-ui-1.10.4.custom/css/ui-lightness/jquery-ui-1.10.4.custom.min'); ?> 
        <?= $this->Html->css('../vendors/font-awesome/css/font-awesome.min'); ?> 
        <?= $this->Html->css('../vendors/bootstrap/css/bootstrap.min'); ?> 
        <?= $this->Html->css('../vendors/jplist/html/css/jplist-custom'); ?> 
        <!--LOADING STYLESHEET FOR PAGE--><!--Loading style vendors-->
        <?= $this->Html->css('../vendors/animate.css/animate'); ?> 
        <?= $this->Html->css('../vendors/jquery-pace/pace'); ?> 
        <?= $this->Html->css('../vendors/iCheck/skins/all'); ?> 
        <?= $this->Html->css('../vendors/jquery-news-ticker/jquery.news-ticker'); ?> 
        <!--Loading style-->
        <?= $this->Html->css('themes/style3/pink-blue'); ?> 
        <?= $this->Html->css('style-responsive.css'); ?> 
        
        <?= $this->Html->script('jquery-1.10.2.min.js', array('inline' => false)); ?>    
        <?= $this->Html->script('jquery-migrate-1.2.1.min.js', array('inline' => false)); ?>    
        <?= $this->Html->script('jquery-ui.js', array('inline' => false)); ?>   
    </head>
    <body>
        <div><!--BEGIN BACK TO TOP--><a id="totop" href="#"><i class="fa fa-angle-up"></i></a><!--END BACK TO TOP-->
            <!--BEGIN TOPBAR-->
            <div id="header-topbar-option-demo" class="page-header-topbar">
                <?= $this->element('header', array('user' => $currentuser)); ?>  
            </div>
            <!--END TOPBAR-->
            <div id="wrapper">
                <!--BEGIN SIDEBAR MENU-->
                <?= $this->element('sidebar', array('user' => $currentuser, 'menu' => $menu)); ?>  
                <!--END SIDEBAR MENU--><!--BEGIN PAGE WRAPPER-->
                <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
                    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                        <div class="page-header pull-left">
                            <div class="page-title"><?= $currentuser['user_type'] ?></div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li><i class="fa fa-home"></i>&nbsp;<a href="/spriseAdmin/users">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                            <li class="active"><div class="page-title"><?= $this->fetch('title') ?></div></li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                    <!--END TITLE & BREADCRUMB PAGE--><!--BEGIN CONTENT-->
                    <div class="page-content">
                        <?= $this->fetch('content') ?>
                    </div>
                    <!--END CONTENT--><!--BEGIN FOOTER-->
                    <div id="footer">
                        <?= $this->element('footer'); ?>  
                    </div>
                    <!--END FOOTER--></div>
                <!--END PAGE WRAPPER--></div>
        </div>

         
        <!--loading bootstrap js-->
        <?= $this->Html->script('../vendors/bootstrap/js/bootstrap.min.js', array('inline' => false)); ?> 
        <?= $this->Html->script('../vendors/bootstrap/js/bootstrap.min.js', array('inline' => false)); ?> 
        <?= $this->Html->script('../vendors/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js', array('inline' => false)); ?> 
        <?= $this->Html->script('html5shiv.js', array('inline' => false)); ?> 
        <?= $this->Html->script('respond.min.js', array('inline' => false)); ?> 
        <?= $this->Html->script('../vendors/metisMenu/jquery.metisMenu.js', array('inline' => false)); ?> 
        <?= $this->Html->script('../vendors/slimScroll/jquery.slimscroll.js', array('inline' => false)); ?> 
        <?= $this->Html->script('../vendors/jquery-cookie/jquery.cookie.js', array('inline' => false)); ?> 
        <?= $this->Html->script('../vendors/iCheck/icheck.min.js', array('inline' => false)); ?> 
        <?= $this->Html->script('../vendors/iCheck/custom.min.js', array('inline' => false)); ?> 
        <?= $this->Html->script('../vendors/jquery-news-ticker/jquery.news-ticker.js'); ?> 
        <?= $this->Html->script('jquery.menu.js', array('inline' => false)); ?> 
        <?= $this->Html->script('../vendors/jquery-pace/pace.min.js', array('inline' => false)); ?> 
        <?= $this->Html->script('../vendors/holder/holder.js', array('inline' => false)); ?> 
        <?= $this->Html->script('../vendors/responsive-tabs/responsive-tabs.js', array('inline' => false)); ?> 
        <!--LOADING SCRIPTS FOR PAGE-->
        <!--CORE JAVASCRIPT-->
        <?= $this->Html->script('main.js', array('inline' => false)); ?> 


    </body>
</html>