<div id="generalTabContent" class="tab-content">
    <div id="tab-edit" class="tab-pane fade in active">

        <?= $this->Form->create($cm, ['class' => 'form-horizontal', 'type' => 'file']) ?>

        <div class="form-group"><label class="col-sm-3 control-label">About Us</label>

            <div class="col-sm-9 controls">
                <div class="row">
                    <div class="col-xs-9">
                        <?= $this->Form->control('about_us', ['label' => false, 'class' => 'form-control', 'placeholder' => 'About Us']); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group"><label class="col-sm-3 control-label">Terms & Conditions</label>

            <div class="col-sm-9 controls">
                <div class="row">
                    <div class="col-xs-9">
                        <?= $this->Form->control('term_and_conditins', ['label' => false, 'placeholder' => 'Terms & Conditins', 'class' => ['form-control']]); ?>

                    </div>
                </div>
            </div>
        </div>
        
        <div class="form-group"><label class="col-sm-3 control-label"></label>
            <div class="col-sm-9 controls">
                <div class="row">
                    <div class="col-xs-9">
                        <hr/>
                        <?= $this->Form->button(__('Submit'), ['class' => ['btn', 'btn-green', 'btn-block']]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>