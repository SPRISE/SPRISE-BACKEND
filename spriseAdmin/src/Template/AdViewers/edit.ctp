<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdViewer $adViewer
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $adViewer->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $adViewer->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Ad Viewers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Advertisements'), ['controller' => 'Advertisements', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Advertisement'), ['controller' => 'Advertisements', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="adViewers form large-9 medium-8 columns content">
    <?= $this->Form->create($adViewer) ?>
    <fieldset>
        <legend><?= __('Edit Ad Viewer') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->control('ad_id', ['options' => $advertisements, 'empty' => true]);
            echo $this->Form->control('viewed_at', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
