<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdViewer $adViewer
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ad Viewer'), ['action' => 'edit', $adViewer->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ad Viewer'), ['action' => 'delete', $adViewer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $adViewer->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ad Viewers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ad Viewer'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Advertisements'), ['controller' => 'Advertisements', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Advertisement'), ['controller' => 'Advertisements', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="adViewers view large-9 medium-8 columns content">
    <h3><?= h($adViewer->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $adViewer->has('user') ? $this->Html->link($adViewer->user->name, ['controller' => 'Users', 'action' => 'view', $adViewer->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Advertisement') ?></th>
            <td><?= $adViewer->has('advertisement') ? $this->Html->link($adViewer->advertisement->title, ['controller' => 'Advertisements', 'action' => 'view', $adViewer->advertisement->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($adViewer->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Viewed At') ?></th>
            <td><?= h($adViewer->viewed_at) ?></td>
        </tr>
    </table>
</div>
