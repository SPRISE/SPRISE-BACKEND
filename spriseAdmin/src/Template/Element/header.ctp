
<!DOCTYPE html>
<nav id="topbar" role="navigation" style="margin-bottom: 0;" data-step="3" data-intro="&lt;b&gt;Topbar&lt;/b&gt; has other styles with live demo. Go to &lt;b&gt;Layouts-&gt;Header&amp;Topbar&lt;/b&gt; and check it out." class="navbar navbar-default navbar-static-top">
    <div class="navbar-header">
        <button type="button" data-toggle="collapse" data-target=".sidebar-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        <a id="logo" href="/spriseAdmin/users" class="navbar-brand"><span class="fa fa-rocket"></span><span class="logo-text">S'prise</span><span style="display: none" class="logo-text-icon">s</span></a></div>
    <div class="topbar-main"><a id="menu-toggle" href="#" class="hidden-xs"><i class="fa fa-bars"></i></a>
        <ul class="nav navbar navbar-top-links navbar-right mbn">
            <li class="dropdown topbar-user">
                <a data-hover="dropdown" href="#" class="dropdown-toggle">
                    <img src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/48.jpg" alt="" class="img-responsive img-circle"/>&nbsp;<span class="hidden-xs">
                        <?= $currentuser['name']  ?>
                    </span>&nbsp;<span class="caret"></span></a>
                <ul class="dropdown-menu dropdown-user pull-right">
                    <li>
<!--                        <a href="extra-signin.html"><i class="fa fa-key"></i></a>-->
                        <?= $this->Html->link('Logout', ['controller' => 'Users', 'action' => 'logout']) ?>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>                                   