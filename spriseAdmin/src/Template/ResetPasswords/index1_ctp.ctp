<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<br>
<div class="index view large-4 medium-4 large-offset-4 medium-offset-4 columns">
    <div class="panel">
        <h2 class="text-center">Login</h2>
        <?= $this->Form->create(); ?>
        <div class="form-group"><label for="inputFirstName" class="col-md-3 control-label">E-mail <span class='require'>*</span></label>

            <div class="col-md-9">
                <?= $this->Form->control('email', ['label' => false, 'Required' => 'Required', 'placeholder' => 'E-mail', 'class' => 'form-control']); ?>
            </div>
        </div>
        <div class="form-group"><label for="inputFirstName" class="col-md-3 control-label">Password <span class='require'>*</span></label>

            <div class="col-md-9">
                <?= $this->Form->control('password', ['label' => false, 'Required' => 'Required', 'placeholder' => 'Password ', 'class' => 'form-control']); ?>
            </div>
        </div>
        <?= $this->Form->submit('Login', array('class' => 'button')); ?>
        <?= $this->Form->end(); ?>
    </div>
</div>
