<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ResetPassword $resetPassword
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Reset Password'), ['action' => 'edit', $resetPassword->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Reset Password'), ['action' => 'delete', $resetPassword->id], ['confirm' => __('Are you sure you want to delete # {0}?', $resetPassword->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Reset Passwords'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Reset Password'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="resetPasswords view large-9 medium-8 columns content">
    <h3><?= h($resetPassword->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Password Id') ?></th>
            <td><?= h($resetPassword->password_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $resetPassword->has('user') ? $this->Html->link($resetPassword->user->name, ['controller' => 'Users', 'action' => 'view', $resetPassword->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($resetPassword->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($resetPassword->created_at) ?></td>
        </tr>
    </table>
</div>
