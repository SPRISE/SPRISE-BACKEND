<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<br>
<div class="index view large-4 medium-4 large-offset-4 medium-offset-4 columns">
    <div class="panel">
        <h2 class="text-center">Reset Password</h2>
        <?= $this->Form->create(); ?>
        <div class="form-group"><label for="inputFirstName" class="col-md-3 control-label">Password <span class='require'>*</span></label>

            <div class="col-md-9">
                <?= $this->Form->control('password', ['type'=>'password','label' => false, 'Required' => 'Required', 'placeholder' => 'E-mail', 'class' => 'form-control']); ?>
            </div>
        </div>
        <div class="form-group"><label for="inputFirstName" class="col-md-3 control-label">Confirm Password <span class='require'>*</span></label>

            <div class="col-md-9">
                <?= $this->Form->control('confirm_password', ['type'=>'password','label' => false, 'Required' => 'Required', 'placeholder' => 'Password ', 'class' => 'form-control']); ?>
            </div>
        </div>
        <?= $this->Form->submit('Reset Password', array('class' => 'button')); ?>
        <?= $this->Form->end(); ?>
    </div>
</div>
