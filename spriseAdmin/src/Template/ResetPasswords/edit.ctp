<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ResetPassword $resetPassword
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $resetPassword->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $resetPassword->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Reset Passwords'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="resetPasswords form large-9 medium-8 columns content">
    <?= $this->Form->create($resetPassword) ?>
    <fieldset>
        <legend><?= __('Edit Reset Password') ?></legend>
        <?php
            echo $this->Form->control('password_id');
            echo $this->Form->control('user_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->control('created_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
