<div id="table-advanced" class="row">
    <div class="col-lg-12">

        <div  class="tab-content">

            <div id="table-sticky-tab" >

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row mbm">
                            <div class="col-lg-6">
                                <div class="pagination-panel">Page
                                    &nbsp;<a href="#" class="btn btn-sm btn-default btn-prev"><i class="fa fa-angle-left"></i></a>&nbsp;<input type="text" maxlenght="5" value="1" class="pagination-panel-input form-control input-mini input-inline input-sm text-center"/>&nbsp;<a href="#" class="btn btn-sm btn-default btn-prev"><i class="fa fa-angle-right"></i></a>&nbsp;
                                    of 6 | View
                                    &nbsp;<select class="form-control input-xsmall input-sm input-inline">
                                        <option value="20" selected="selected">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                        <option value="150">150</option>
                                        <option value="-1">All</option>
                                    </select>&nbsp;
                                    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                                </div>
                            </div>
                            <div class="col-lg-6 text-right">
                                <div class="pagination-panel">
                                    <ul class="pagination pagination-sm man">
                                        <?= $this->Paginator->first('<< ' . __('first')) ?>
                                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next(__('next') . ' >') ?>
                                        <?= $this->Paginator->last(__('last') . ' >>') ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <table class="table table-hover table-striped table-bordered table-advanced tablesorter tb-sticky-header">
                            <thead>
                                <tr>
                                    <th width="3%"><input type="checkbox" class="checkall"/></th>
                                    <th width="9%">Record #</th>
                                    <th><?= $this->Paginator->sort('name') ?></th>
                                    <th width="10%"><?= $this->Paginator->sort('email') ?></th>
                                    <th width="10%"><?= $this->Paginator->sort('gender') ?></th>
                                    <th width="7%"><?= $this->Paginator->sort('city_id') ?></th>
                                    <th width="7%"><?= $this->Paginator->sort('state_id') ?></th>
                                    <th width="7%"><?= $this->Paginator->sort('contact_no') ?></th>
                                    <th width="12%"><?= $this->Paginator->sort('created_at') ?></th>
                                    <th width="10%"><?= $this->Paginator->sort('user_type') ?></th>
                                    <th width="9%"><?= $this->Paginator->sort('status') ?></th>
                                    <th width="12%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($users as $user): ?>
                                    <tr>
                                        <td><input type="checkbox"/></td>
                                        <td><?= $this->Number->format($user->id) ?></td>
                                        <td><?= h($user->name) ?></td>
                                        <td><?= h($user->email) ?></td>
                                        <td><?= h($user->gender) ?></td>
                                        <td><?= $user->has('city') ? $this->Html->link($user->city->title, ['controller' => 'Cities', 'action' => 'view', $user->city->id]) : '' ?></td>
                                        <td><?= $user->has('state') ? $this->Html->link($user->state->title, ['controller' => 'States', 'action' => 'view', $user->state->id]) : '' ?></td>
                                        <td><?= h($user->contact_no) ?></td>
                                        <td><?= h($user->created_at) ?></td>
                                        <td><?= h($user->user_type) ?></td>
                                        <td><span class="label label-sm <?php
                                            if ($user->status === 'ACTIVE') {
                                                echo 'label-success';
                                            } else {
                                               echo 'label-danger';
                                            }
                                            ?>"><?= h($user->status) ?></span></td>
                                        <td><i class="fa fa-edit">
                                                    <?= $this->Html->link(__('Edit'), ['controller' => 'users', 'action' => 'edit', $user->id]) ?>
                                                </i>
                                            </td>
                                    </tr>
<?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--LOADING SCRIPTS FOR PAGE User List Page-->
<?= $this->Html->script('../vendors/jquery-tablesorter/jquery.tablesorter.js', array('inline' => false)); ?> 
<?= $this->Html->script('table-advanced.js', array('inline' => false)); ?> 