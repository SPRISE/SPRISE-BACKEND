<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
    <div class="col-md-12">

        <div class="row mtl">
            
            <div class="col-md-9">
                
                <div  class="tab-content">
                    <div id="tab-edit" class="tab-pane fade in active">

                        <?= $this->Form->create($user, ['class' => 'form-horizontal']) ?>
                        <h3>Account Setting</h3>

                        <div class="form-group"><label class="col-sm-3 control-label">Email</label>

                            <div class="col-sm-9 controls">
                                <div class="row">
                                    <div class="col-xs-9">
                                        <?= $this->Form->control('email', ['label' => false, 'Required' => 'Required','class' => 'form-control', 'placeholder' => 'email@yourcompany.com']); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">Password</label>

                            <div class="col-sm-9 controls">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <?=
                                        $this->Form->control('password', ['value'=>'','label' => false,
                                            'placeholder' => 'password',
                                            'type' => 'password',
                                            'class' => 'form-control']);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">Confirm Password</label>

                            <div class="col-sm-9 controls">
                                <div class="row">
                                    <div class="col-xs-4"><input type="password" placeholder="password" class="form-control"/></div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <h3>Profile Setting</h3>

                        <div class="form-group"><label class="col-sm-3 control-label">Name</label>

                            <div class="col-sm-9 controls">
                                <div class="row">
                                    <div class="col-xs-9">
                                        <?= $this->Form->control('name', ['Required' => 'Required','label' => false, 'class' => 'form-control', 'placeholder' => 'Name']); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">Gender</label>

                            <div class="col-sm-9 controls">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <?php
                                        $options_gender = ['Male' => 'Male', 'Female' => 'Female'];
                                        echo $this->Form->control('gender', ['label' => false, 'options' => $options_gender, 'empty' => false, 'class' => 'form-control'], 'Male');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">Status</label>

                            <div class="col-sm-9 controls">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <?php
                                        $options_status = ['ACTIVE' => 'Active', 'DEACTIVE' => 'Inactive'];
                                        echo $this->Form->control('status', ['Required' => 'Required','label' => false, 'options' => $options_status, 'empty' => false, 'class' => 'form-control']);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">State</label>

                            <div class="col-sm-9 controls">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <?= $this->Form->control('state_id', ['label' => false, 'options' => $states, 'empty' => true, 'class' => 'form-control']); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-sm-3 control-label">City</label>

                            <div class="col-sm-9 controls">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <?= $this->Form->control('city_id', ['label' => false, 'options' => $cities, 'empty' => true, 'class' => 'form-control']); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <hr/>
                        <h3>Contact Setting</h3>

                        <div class="form-group"><label class="col-sm-3 control-label">Contact No</label>

                            <div class="col-sm-9 controls">
                                <div class="row">
                                    <div class="col-xs-9">
                                        <?= $this->Form->control('contact_no', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Phone Number']); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <?= $this->Form->button(__('Submit'), ['class' => ['btn', 'btn-green', 'btn-block']]) ?>
                        <?= $this->Form->end() ?>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('app/app.js'); ?>    