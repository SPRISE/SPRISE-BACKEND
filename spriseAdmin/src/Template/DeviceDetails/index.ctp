<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeviceDetail[]|\Cake\Collection\CollectionInterface $deviceDetails
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Device Detail'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="deviceDetails index large-9 medium-8 columns content">
    <h3><?= __('Device Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('device_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('token') ?></th>
                <th scope="col"><?= $this->Paginator->sort('userid') ?></th>
                <th scope="col"><?= $this->Paginator->sort('logged_in') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($deviceDetails as $deviceDetail): ?>
            <tr>
                <td><?= $this->Number->format($deviceDetail->id) ?></td>
                <td><?= h($deviceDetail->type) ?></td>
                <td><?= h($deviceDetail->device_id) ?></td>
                <td><?= h($deviceDetail->token) ?></td>
                <td><?= $this->Number->format($deviceDetail->userid) ?></td>
                <td><?= h($deviceDetail->logged_in) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $deviceDetail->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $deviceDetail->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $deviceDetail->id], ['confirm' => __('Are you sure you want to delete # {0}?', $deviceDetail->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
