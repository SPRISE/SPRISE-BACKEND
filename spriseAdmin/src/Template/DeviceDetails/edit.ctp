<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeviceDetail $deviceDetail
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $deviceDetail->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $deviceDetail->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Device Details'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="deviceDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($deviceDetail) ?>
    <fieldset>
        <legend><?= __('Edit Device Detail') ?></legend>
        <?php
            echo $this->Form->control('type');
            echo $this->Form->control('device_id');
            echo $this->Form->control('token');
            echo $this->Form->control('userid');
            echo $this->Form->control('logged_in');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
