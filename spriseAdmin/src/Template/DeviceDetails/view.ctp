<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeviceDetail $deviceDetail
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Device Detail'), ['action' => 'edit', $deviceDetail->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Device Detail'), ['action' => 'delete', $deviceDetail->id], ['confirm' => __('Are you sure you want to delete # {0}?', $deviceDetail->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Device Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Device Detail'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="deviceDetails view large-9 medium-8 columns content">
    <h3><?= h($deviceDetail->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($deviceDetail->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Device Id') ?></th>
            <td><?= h($deviceDetail->device_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Token') ?></th>
            <td><?= h($deviceDetail->token) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($deviceDetail->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Userid') ?></th>
            <td><?= $this->Number->format($deviceDetail->userid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Logged In') ?></th>
            <td><?= $deviceDetail->logged_in ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
