<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Advertisement $advertisement
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $advertisement->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $advertisement->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Advertisements'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List States'), ['controller' => 'States', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New State'), ['controller' => 'States', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cities'), ['controller' => 'Cities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New City'), ['controller' => 'Cities', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="advertisements form large-9 medium-8 columns content">
    <?= $this->Form->create($advertisement) ?>
    <fieldset>
        <legend><?= __('Edit Advertisement') ?></legend>
        <?php
            echo $this->Form->control('uuid');
            echo $this->Form->control('title');
            echo $this->Form->control('description');
            echo $this->Form->control('image_url');
            echo $this->Form->control('embded_link');
            echo $this->Form->control('state_id', ['options' => $states, 'empty' => true]);
            echo $this->Form->control('city_id', ['options' => $cities, 'empty' => true]);
            echo $this->Form->control('schedule_date');
            echo $this->Form->control('animation_type');
            echo $this->Form->control('created_by');
            echo $this->Form->control('status');
            echo $this->Form->control('why_rejected');
            echo $this->Form->control('created_at');
            echo $this->Form->control('modified_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
