<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Advertisement[]|\Cake\Collection\CollectionInterface $advertisements
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-body">
                <div id="grid-layout-div" class="box jplist">
                    <div class="jplist-ios-button"><i class="fa fa-sort"></i>jPList Actions</div>
                    <div class="jplist-panel box panel-top">

                        <?php
                        if ($currentuser['user_type'] === 'BRAND') {
                            echo $this->Html->link(__('Add Advertisement', true), array('action' => 'add'), array('class' => ['jplist-reset-btn', 'btn btn-default', 'btn-info']));
                        }
                        ?></a>&nbsp;
                        <button type="button" data-control-type="reset" data-control-name="reset" data-control-action="reset" class="btn btn-default jplist-reset-btn">Reset<i class="fa fa-share mls"></i></button>
                        <div class="text-filter-box">
                            <div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input data-path=".title" type="text" value="" placeholder="Filter by Title" data-control-type="textbox" data-control-name="title-filter" data-control-action="filter" class="form-control"/></div>
                        </div>
                        <div class="text-filter-box">
                            <div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input data-path=".desc" type="text" value="" placeholder="Filter by Description" data-control-type="textbox" data-control-name="desc-filter" data-control-action="filter" class="form-control"/></div>
                        </div>
                        <div data-type="Page {current} of {pages}" data-control-type="pagination-info" data-control-name="paging" data-control-action="paging" class="jplist-label btn btn-default"></div>
                        <div data-control-type="pagination" data-control-name="paging" data-control-action="paging" class="jplist-pagination"></div>
                    </div>
                    <div class="list box text-shadow" style="margin-top: 40px;">
                        <?php foreach ($advertisements as $advertisement): ?>
                        <div class="list-item box" style="min-height: 200px; width: 100%; margin-top: 40px;">
                                <div class="img left"><img src="<?= h($advertisement->image_url) ?>" alt="" title=""/>
                                </div>
                                <?= $this->Html->link(__('Edit Advertisement'), ['action' => 'edit', $advertisement->id]) ?>
                                <div class="block right">
                                    
                                    <p><?= h($advertisement->schedule_date) ?></p>
                                    <p class="title"><?= h($advertisement->title) ?></p>
                                    <p class="desc"><?= h($advertisement->description) ?></p>
                                    <p class="like"><span class="label label-sm <?php
                                            if ($advertisement->status === 'ACTIVE') {
                                                echo 'label-success';
                                            } else {
                                               echo 'label-danger';
                                            }
                                            ?>"><?= h($advertisement->status) ?></span></p></div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="box jplist-no-results text-shadow align-center"><p>No results found</p></div>
                    <div class="jplist-ios-button"><i class="fa fa-sort"></i>jPList Actions</div>
                    <div class="jplist-panel box panel-bottom">
                        <div data-control-type="drop-down" data-control-name="paging" data-control-action="paging" data-control-animate-to-top="true" class="jplist-drop-down form-control">
                            <ul class="dropdown-menu">
                                <li><span data-number="3"> 3 per page</span></li>
                                <li><span data-number="5"> 5 per page</span></li>
                                <li><span data-number="10" data-default="true"> 10 per page</span></li>
                                <li><span data-number="all"> view all</span></li>
                            </ul>
                        </div>
                        <div data-control-type="drop-down" data-control-name="sort" data-control-action="sort" data-control-animate-to-top="true" data-datetime-format="{month}/{day}/{year}" class="jplist-drop-down form-control">
                            <ul class="dropdown-menu">
                                <li><span data-path="default">Sort by</span></li>
                                <li><span data-path=".title" data-order="asc" data-type="text">Title A-Z</span></li>
                                <li><span data-path=".title" data-order="desc" data-type="text">Title Z-A</span></li>
                                <li><span data-path=".desc" data-order="asc" data-type="text">Description A-Z</span></li>
                                <li><span data-path=".desc" data-order="desc" data-type="text">Description Z-A</span></li>
                                <li><span data-path=".like" data-order="asc" data-type="number" data-default="true">Likes asc</span></li>
                                <li><span data-path=".like" data-order="desc" data-type="number">Likes desc</span></li>
                                <li><span data-path=".date" data-order="asc" data-type="datetime">Date asc</span></li>
                                <li><span data-path=".date" data-order="desc" data-type="datetime">Date desc</span></li>
                            </ul>
                        </div>
                        <div data-type="{start} - {end} of {all}" data-control-type="pagination-info" data-control-name="paging" data-control-action="paging" class="jplist-label btn btn-default"></div>
                        <div data-control-animate-to-top="true" data-control-type="pagination" data-control-name="paging" data-control-action="paging" class="jplist-pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--LOADING SCRIPTS FOR PAGE-->
<?= $this->Html->script('../vendors/jplist/html/js/vendor/modernizr.min.js', array('inline' => false)); ?> 
<?= $this->Html->script('../vendors/jplist/html/js/jplist.min.js', array('inline' => false)); ?> 
<?= $this->Html->script('jplist.js', array('inline' => false)); ?> 

