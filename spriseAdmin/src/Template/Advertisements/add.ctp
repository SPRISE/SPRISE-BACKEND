<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Advertisement $advertisement
 */
?>
<?= $this->Html->css('../vendors/bootstrap-datepicker/css/datepicker3'); ?>

<div id="generalTabContent" class="tab-content">
    <div id="tab-edit" class="tab-pane fade in active">

        <?= $this->Form->create($advertisement, ['class' => 'form-horizontal', 'type' => 'file']) ?>
        <h3>New Advertisement</h3>

        <div class="form-group"><label class="col-sm-3 control-label">Title</label>

            <div class="col-sm-9 controls">
                <div class="row">
                    <div class="col-xs-9">
                        <?= $this->Form->control('title', ['Required' => 'Required', 'label' => false, 'class' => 'form-control', 'placeholder' => 'Title']); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group"><label class="col-sm-3 control-label">Description</label>
            <div class="col-sm-9 controls">
                <div class="row">
                    <div class="col-xs-9">
                        <?= $this->Form->control('description', ['Required' => 'Required', 'label' => false, 'class' => 'form-control', 'placeholder' => 'description']); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group"><label class="col-sm-3 control-label">Image URL</label>

            <div class="col-sm-9 controls">
                <div class="row">
                    <div class="col-xs-9">
                        <?= $this->Form->control('file_path', ['Required' => 'Required', 'type' => 'file', 'label' => false, 'placeholder' => 'Company Name', 'class' => ['form-control', 'btn']]); ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="form-group"><label class="col-sm-3 control-label">Embedded Link</label>

            <div class="col-sm-9 controls">
                <div class="row">
                    <div class="col-xs-9">
                        <?=
                        $this->Form->control('embded_link', ['Required' => 'Required', 'label' => false,
                            'placeholder' => 'Embedded Link',
                            'type' => 'text',
                            'class' => 'form-control']);
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group"><label class="col-sm-3 control-label">Schedule Date</label>

            <div class="col-sm-9 controls">
                <div class="row">
                    <div class="col-xs-9">
                        <div data-date-format="dd/mm/yyyy" class="input-group date datepicker-filter mbs">
                            <?= $this->Form->control('schedule_date', ['Required' => 'Required', 'label' => false, 'class' => 'date-picker form-control', 'placeholder' => 'Select Schedule Date']); ?>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>

                    </div>
                </div>
            </div>
        </div>
        <div class="form-group"><label class="col-sm-3 control-label">Animation Type</label>

            <div class="col-sm-9 controls">
                <div class="row">
                    <div class="col-xs-9">
                        <?php
                        $options_gender = ['Envelope' => 'Envelope', 'GifBox' => 'GifBox'];
                        echo $this->Form->control('animation_type', ['Required' => 'Required', 'label' => false, 'options' => $options_gender, 'empty' => false, 'class' => 'form-control']);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group"><label class="col-sm-3 control-label">Status</label>

            <div class="col-sm-9 controls">
                <div class="row">
                    <div class="col-xs-9">
                        <?php
                        $options_status = ['ACTIVE' => 'Active', 'DEACTIVE' => 'Inactive'];
                        echo $this->Form->control('status', ['Required' => 'Required', 'label' => false, 'options' => $options_status, 'empty' => false, 'class' => 'form-control']);
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group"><label class="col-sm-3 control-label">State</label>

            <div class="col-sm-9 controls">
                <div class="row">
                    <div class="col-xs-9">
                        <?= $this->Form->control('state_id', ['Required' => 'Required', 'label' => false, 'options' => $states, 'empty' => true, 'class' => 'form-control']); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group"><label class="col-sm-3 control-label">City</label>

            <div class="col-sm-9 controls">
                <div class="row">
                    <div class="col-xs-9">
                        <?= $this->Form->control('city_id', ['Required' => 'Required', 'label' => false, 'options' => [], 'empty' => true, 'class' => 'form-control']); ?>
                    </div>
                </div>
            </div>
        </div>


        <hr/>
        <div class="form-group"><label class="col-sm-3 control-label"></label>

            <div class="col-sm-9 controls">
                <div class="row">
                    <div class="col-xs-9">
                        <?= $this->Form->button(__('Submit'), ['class' => ['btn', 'btn-green', 'btn-block']]) ?>
                    </div>
                </div>
            </div>
        </div>
        
        <?= $this->Form->end() ?>
    </div>
</div>
<?= $this->Html->script('app/app.js'); ?>  
<?= $this->Html->script('../vendors/bootstrap-datepicker/js/bootstrap-datepicker.js', array('inline' => false)); ?>  