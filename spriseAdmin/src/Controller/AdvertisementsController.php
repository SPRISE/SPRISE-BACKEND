<?php
namespace App\Controller;

use App\Controller\AppController;
use \Cake\ORM\TableRegistry;
/**
 * Advertisements Controller
 *
 * @property \App\Model\Table\AdvertisementsTable $Advertisements
 *
 * @method \App\Model\Entity\Advertisement[] paginate($object = null, array $settings = [])
 */
class AdvertisementsController extends AppController
{

    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $this->paginate = [
            'contain' => ['States', 'Cities']
        ];
        $this->paginate['order'] = array('Advertisements.created_at' => 'desc');

        $user = $this->Auth->user();

        if ($user['user_type'] === 'BRAND') {
            $this->paginate = [ 'contain' => ['States', 'Cities'],
                'conditions' => [
                    'created_by' => $user['id']
                ],[]
            ];
        }
        $advertisements = $this->paginate($this->Advertisements);
        $this->set(compact('advertisements'));
        $this->set('_serialize', ['advertisements']);
    }

    /**
     * View method
     *
     * @param string|null $id Advertisement id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $advertisement = $this->Advertisements->get($id, [
            'contain' => ['States', 'Cities']
        ]);

        $this->set('advertisement', $advertisement);
        $this->set('_serialize', ['advertisement']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $advertisement = $this->Advertisements->newEntity();
         
         $currentuser = $this->Auth->user();
        if ($this->request->is('post')) {
            $fileName = $this->request->data['file_path']['name'];
            $uploadPath = 'uploads/ads/';
            $uploadFile = $uploadPath .'_'.time().$fileName;
            if (move_uploaded_file($this->request->data['file_path']['tmp_name'], $uploadFile)) {
                $advertisement['image_url']=$uploadFile;
            }
                $advertisement = $this->Advertisements->patchEntity($advertisement, $this->request->getData());
                $now = date('Y-m-d H:i:s');
                $advertisement['image_url']=$uploadFile;
                $advertisement['status']='ACTIVE';
                $advertisement['created_at']=$now;
                $advertisement['modified_at']=$now;
                $advertisement['created_by']=$currentuser['id'];
                $advertisement['uuid'] = $this->Uuid->v4();
               
                if ($this->Advertisements->save($advertisement)) {
                    
//                             'Users.state_id'=>$advertisement['state_id'],
  //                           'Users.city_id'=>$advertisement['city_id'],
                    // TODO We need to add GroupBy here on device_id
                    $device_detail_table = TableRegistry::get('DeviceDetails');
                    $allDeviceData = $device_detail_table->find('all', ['contain' => ['Users'],
                    'conditions' => [
                        'Users.state_id' => $advertisement['state_id'],
                        'Users.city_id' => $advertisement['city_id'],
                        'Users.user_type' => 'USER',
                        'logged_in' => 1
                    ]
                ]);

                $brands_table = TableRegistry::get('Brands');
                    $brand_data=$brands_table->find('all',[
                         'conditions'=>['user_id'=>$currentuser['id']]
                    ]);
                    
                    $bran_name=$brand_data->first();
                   
                    foreach ($allDeviceData as $devicedetail) {
                        $message=$bran_name['company_name'].' has created a new add '.$advertisement['title'].' in your city.';
                        if ($devicedetail['type'] === 'A') {
                            $this->Notification->pushNotification($devicedetail['token'], $message, 'ADS', $advertisement['id']);
                        } else {
                            $this->Notification->pushNotificationIOS($devicedetail['token'], $message,$devicedetail['mode']);
                        }
                    }

                    return $this->redirect(['action' => 'index']);
                    
                }
                $this->Flash->error(__('The advertisement could not be saved. Please, try again.'));
            
        }
       
        $states = $this->Advertisements->States->find('list', ['order'=>'States.title']);
        $this->set(compact('advertisement', 'states'));
        $this->set('_serialize', ['advertisement']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Advertisement id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $advertisement = $this->Advertisements->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $advertisement = $this->Advertisements->patchEntity($advertisement, $this->request->getData());
            $fileName = $this->request->data['file_path']['name'];
            $uploadPath = 'uploads/ads/';
            $uploadFile = $uploadPath .'_'.time().$fileName;
            
            if (!empty($fileName) && move_uploaded_file($this->request->data['file_path']['tmp_name'], $uploadFile)) {
                $advertisement['image_url']=$uploadFile;
            }
            if ($this->Advertisements->save($advertisement)) {
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The advertisement could not be saved. Please, try again.'));
        }
        $states = $this->Advertisements->States->find('list', ['order'=>'States.title']);
        $cities = $this->Advertisements->Cities->find('list', [
            'conditions' => ['Cities.state_id' => $advertisement->state_id],
            'order'=>'Cities.title',
            ]);
        
        $this->set(compact('advertisement', 'states', 'cities'));
        $this->set('_serialize', ['advertisement']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Advertisement id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $advertisement = $this->Advertisements->get($id);
        if ($this->Advertisements->delete($advertisement)) {
            $this->Flash->success(__('The advertisement has been deleted.'));
        } else {
            $this->Flash->error(__('The advertisement could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
