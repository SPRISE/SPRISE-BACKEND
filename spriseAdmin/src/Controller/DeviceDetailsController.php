<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DeviceDetails Controller
 *
 * @property \App\Model\Table\DeviceDetailsTable $DeviceDetails
 *
 * @method \App\Model\Entity\DeviceDetail[] paginate($object = null, array $settings = [])
 */
class DeviceDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Devices']
        ];
        $deviceDetails = $this->paginate($this->DeviceDetails);

        $this->set(compact('deviceDetails'));
        $this->set('_serialize', ['deviceDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Device Detail id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $deviceDetail = $this->DeviceDetails->get($id, [
            'contain' => ['Devices']
        ]);

        $this->set('deviceDetail', $deviceDetail);
        $this->set('_serialize', ['deviceDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $deviceDetail = $this->DeviceDetails->newEntity();
        if ($this->request->is('post')) {
            $deviceDetail = $this->DeviceDetails->patchEntity($deviceDetail, $this->request->getData());
            if ($this->DeviceDetails->save($deviceDetail)) {
                $this->Flash->success(__('The device detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The device detail could not be saved. Please, try again.'));
        }
        $devices = $this->DeviceDetails->Devices->find('list', ['limit' => 200]);
        $this->set(compact('deviceDetail', 'devices'));
        $this->set('_serialize', ['deviceDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Device Detail id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $deviceDetail = $this->DeviceDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $deviceDetail = $this->DeviceDetails->patchEntity($deviceDetail, $this->request->getData());
            if ($this->DeviceDetails->save($deviceDetail)) {
                $this->Flash->success(__('The device detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The device detail could not be saved. Please, try again.'));
        }
        $devices = $this->DeviceDetails->Devices->find('list', ['limit' => 200]);
        $this->set(compact('deviceDetail', 'devices'));
        $this->set('_serialize', ['deviceDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Device Detail id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $deviceDetail = $this->DeviceDetails->get($id);
        if ($this->DeviceDetails->delete($deviceDetail)) {
            $this->Flash->success(__('The device detail has been deleted.'));
        } else {
            $this->Flash->error(__('The device detail could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
