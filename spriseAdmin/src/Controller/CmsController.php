<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Cms Controller
 *
 * @property \App\Model\Table\CmsTable $Cms
 *
 * @method \App\Model\Entity\Cm[] paginate($object = null, array $settings = [])
 */
class CmsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $id=1;
        $cm = $this->Cms->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cm = $this->Cms->patchEntity($cm, $this->request->getData());
            if ($this->Cms->save($cm)) {
                $this->Flash->success(__('The cm has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cm could not be saved. Please, try again.'));
        }
        $this->set(compact('cm'));
        $this->set('_serialize', ['cm']);    
    }

    
   
}
