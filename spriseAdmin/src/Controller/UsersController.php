<?php
namespace App\Controller;

use App\Controller\AppController;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * Index method
     * This is dashboard page wheare shows all the Summery of the user
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $currentuser = $this->Auth->user();
        if ($currentuser['user_type'] === 'ADMIN') {
            return $this->redirect(['controller' => 'Brands', 'action' => 'index']);
        } else {
            return $this->redirect(['controller' => 'Advertisements', 'action' => 'index']);
        }
    }
    
    public function usermanagement()
    {
        $this->paginate = [
            'contain' => ['Cities', 'States']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }
    
    
    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Cities', 'States', 'AdViewers']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'usermanagement']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $states = $this->Users->States->find('list', ['order'=>'States.title']);
        $cities = $this->Users->Cities->find('list', ['limit'=>100,'order'=>'Cities.title']);
        $this->set(compact('user', 'cities', 'states'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'usermanagement']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        
        $states = $this->Users->States->find('list', ['order'=>'States.title']);
        $cities = $this->Users->Cities->find('list', ['limit'=>100,'order'=>'Cities.title']);
        $this->set(compact('user', 'cities', 'states'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    //This is for login function
    public function login() {
        $this->viewBuilder()->layout('loginbase');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                if ($user['user_type'] === 'ADMIN') {
                    return $this->redirect(['controller' => 'Brands', 'action' => 'index']);
                } else {
                    return $this->redirect(['controller' => 'Advertisements', 'action' => 'index']);
                }
            } else {
                $this->Flash->error(__('Username or password is incorrect'));
            }
        }
        
    }
    
  public function logout(){
     
      
      $this->Auth->logout();
      return $this->redirect(['controller' => 'Users']);
  }
    
}
