<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class NotificationComponent extends Component {

    function pushNotification($deviceIds, $message, $type, $typeId, $iconImage = "") {
        $api_key = 'AAAAGsx6qOA:APA91bH229xhiUr865PMV6PHqvIliYj3XVPNYan3fuUSKmOghk-7bL4swqfVn7u7lVvJ0dY9L370zFX84KTA3fesgYKUGp-XWWOqfc6pajexf95CziJASFs5NfI5hsY7HpVzC-sl025a'; //get the api key from FCM backend
        $url = 'https://fcm.googleapis.com/fcm/send';
        //$fields = array('registration_ids'  => array($deviceIds));//get the device token from Android 
        $fields = array(
            'to' => $deviceIds,
            'data' => array(
                "message" => $message,
                "typeId" => $typeId,
                "type" => $type,
                "title" => "S'Prise",
                "image" => "",
                "iconImage" => $iconImage
            )
        );
        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );

        // $headers = array( 'Authorization: key=' . $api_key,'Content-Type: application/json');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return 'Curl error: ' . curl_error($ch);
        }
        curl_close($ch);
        $cur_message = json_decode($result);
        return $cur_message;
    }

    function pushNotificationIOS($deviceToken, $message, $mode) {
        $ctx = stream_context_create();
        //$ckpath = SITEURL.'pushcert.pem'; 
        if ($mode === "SANDBOX") {
            stream_context_set_option($ctx, 'ssl', 'local_cert', '/pem/sprise_dev.pem');
        } else {
            stream_context_set_option($ctx, 'ssl', 'local_cert', '/pem/sprise_production.pem');
        }
        // stream_context_set_option($ctx, 'ssl', 'local_cert', 'pem/sprise_production.pem');
        // Put your private key's passphrase here: 
        $passphrase = '';
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        $fpurl = 'ssl://gateway.sandbox.push.apple.com:2195';

        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default',
            'badge' => 0,
            'data' => $message,
        );

        $payload = json_encode($body);
        //print_r($payload);exit;
        try {
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        } catch (Exception $e) {
            return 'FAILURE';
        }
        $fp = null;
        $fp = @stream_socket_client($fpurl, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp)
			return 'FAILURE';
            //exit("Failed to connect: $err $errstr" . PHP_EOL);

        // Send it to the server
        try {
            $result = fwrite($fp, $msg, strlen($msg));
            fclose($fp);
            $fp = null;
            $ctx = null;
        } catch (Exception $e) {
            if (!$fp) {
                fclose($fp);
            }
            $fp = null;
            $ctx = null;
            return 'FAILURE';
        }
        //print_r($result);
        if ($result)
            return 'SUCCESS';
        else
            return 'FAILURE';
    }

}
