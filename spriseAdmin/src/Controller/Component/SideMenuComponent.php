<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class SideMenuComponent extends Component {

    function fetchData($user) {
        //Dynamically Loading the model of the menu, because it's not necessary to have it on every controller.
        $admin_menu=['brands'=>['title'=>'Brands','icon'=>'fa fa-tachometer fa-fw','link'=>'/spriseAdmin/brands'],
            'Ads'=>['title'=>'Ads','icon'=>'fa fa-bullhorn','link'=>'/spriseAdmin/advertisements'],
            'Users'=>['title'=>'Users','icon'=>'fa fa-group','link'=>'/spriseAdmin/users/usermanagement'],
            'CMS'=>['title'=>'CMS','icon'=>'fa fa-bolt','link'=>'/spriseAdmin/cms']
                      ];
        
        $brands_menu=[ 'Ads'=>['title'=>'Ads','icon'=>'fa fa-bullhorn','link'=>'/spriseAdmin/advertisements'],
            
                      ];
        
       
        if (!empty($user)) {
           if ($user['user_type'] === 'ADMIN') {
               return $admin_menu;
            } else if ($user['user_type'] === 'BRAND') {
                return $brands_menu;
            }
        }
        
        return NULL;
    }

}
