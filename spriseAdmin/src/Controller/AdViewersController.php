<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AdViewers Controller
 *
 * @property \App\Model\Table\AdViewersTable $AdViewers
 *
 * @method \App\Model\Entity\AdViewer[] paginate($object = null, array $settings = [])
 */
class AdViewersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Advertisements']
        ];
        $adViewers = $this->paginate($this->AdViewers);

        $this->set(compact('adViewers'));
        $this->set('_serialize', ['adViewers']);
    }

    /**
     * View method
     *
     * @param string|null $id Ad Viewer id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $adViewer = $this->AdViewers->get($id, [
            'contain' => ['Users', 'Advertisements']
        ]);

        $this->set('adViewer', $adViewer);
        $this->set('_serialize', ['adViewer']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $adViewer = $this->AdViewers->newEntity();
        if ($this->request->is('post')) {
            $adViewer = $this->AdViewers->patchEntity($adViewer, $this->request->getData());
            if ($this->AdViewers->save($adViewer)) {
                $this->Flash->success(__('The ad viewer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ad viewer could not be saved. Please, try again.'));
        }
        $users = $this->AdViewers->Users->find('list', ['limit' => 200]);
        $advertisements = $this->AdViewers->Advertisements->find('list', ['limit' => 200]);
        $this->set(compact('adViewer', 'users', 'advertisements'));
        $this->set('_serialize', ['adViewer']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ad Viewer id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $adViewer = $this->AdViewers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $adViewer = $this->AdViewers->patchEntity($adViewer, $this->request->getData());
            if ($this->AdViewers->save($adViewer)) {
                $this->Flash->success(__('The ad viewer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ad viewer could not be saved. Please, try again.'));
        }
        $users = $this->AdViewers->Users->find('list', ['limit' => 200]);
        $advertisements = $this->AdViewers->Advertisements->find('list', ['limit' => 200]);
        $this->set(compact('adViewer', 'users', 'advertisements'));
        $this->set('_serialize', ['adViewer']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ad Viewer id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $adViewer = $this->AdViewers->get($id);
        if ($this->AdViewers->delete($adViewer)) {
            $this->Flash->success(__('The ad viewer has been deleted.'));
        } else {
            $this->Flash->error(__('The ad viewer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
