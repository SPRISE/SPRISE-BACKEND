<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * ResetPasswords Controller
 *
 * @property \App\Model\Table\ResetPasswordsTable $ResetPasswords
 *
 * @method \App\Model\Entity\ResetPassword[] paginate($object = null, array $settings = [])
 */
class ResetPasswordsController extends AppController
{

   
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['action'=>'index']);
    }


    /**

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        
        $pid = $this->request->getQuery('pid');
        
        
        if ($this->request->is('post')) {
            if (isset($pid)) {
                $password = $this->request->getData()['password'];
                $cpass = $this->request->getData()['confirm_password'];
                if ($password != $cpass) {
                    $this->Flash->error(__('The  password does not match. Please, try again.'));
                } else {
                    $resetPassword = $this->ResetPasswords->find('all', [
                        'contain' => ['Users'],
                        'conditions' => [
                            'password_id' => $pid
                        ]
                    ]);
                    $row = $resetPassword->first()->user;
                    $row->password = $password;
                    if ($this->ResetPasswords->Users->save($row)) {
                        $this->Flash->success(__('The password has been changed.'));
                    }
                }
            } else {
                $this->Flash->error(__('Invalid Request.'));
            }
        }
        $this->viewBuilder()->layout('loginbase');
    }

    /**
     * View method
     *
     * @param string|null $id Reset Password id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        debug($this->request);
        die();
        $resetPassword = $this->ResetPasswords->get($id, [
            'contain' => ['Passwords', 'Users']
        ]);

        $this->set('resetPassword', $resetPassword);
        $this->set('_serialize', ['resetPassword']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $resetPassword = $this->ResetPasswords->newEntity();
        if ($this->request->is('post')) {
            $resetPassword = $this->ResetPasswords->patchEntity($resetPassword, $this->request->getData());
            if ($this->ResetPasswords->save($resetPassword)) {
                $this->Flash->success(__('The reset password has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The reset password could not be saved. Please, try again.'));
        }
        $passwords = $this->ResetPasswords->Passwords->find('list', ['limit' => 200]);
        $users = $this->ResetPasswords->Users->find('list', ['limit' => 200]);
        $this->set(compact('resetPassword', 'passwords', 'users'));
        $this->set('_serialize', ['resetPassword']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Reset Password id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $resetPassword = $this->ResetPasswords->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $resetPassword = $this->ResetPasswords->patchEntity($resetPassword, $this->request->getData());
            if ($this->ResetPasswords->save($resetPassword)) {
                $this->Flash->success(__('The reset password has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The reset password could not be saved. Please, try again.'));
        }
        $passwords = $this->ResetPasswords->Passwords->find('list', ['limit' => 200]);
        $users = $this->ResetPasswords->Users->find('list', ['limit' => 200]);
        $this->set(compact('resetPassword', 'passwords', 'users'));
        $this->set('_serialize', ['resetPassword']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Reset Password id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $resetPassword = $this->ResetPasswords->get($id);
        if ($this->ResetPasswords->delete($resetPassword)) {
            $this->Flash->success(__('The reset password has been deleted.'));
        } else {
            $this->Flash->error(__('The reset password could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
