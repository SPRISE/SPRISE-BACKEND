<?php

namespace App\Controller;

use App\Controller\AppController;
use \Cake\ORM\TableRegistry;

/**
 * Brands Controller
 *
 * @property \App\Model\Table\BrandsTable $Brands
 *
 * @method \App\Model\Entity\Brand[] paginate($object = null, array $settings = [])
 */
class BrandsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Cities', 'States', 'Users']
        ];
        $brands = $this->paginate($this->Brands);

        $this->set(compact('brands'));
        $this->set('_serialize', ['brands']);
    }

    /**
     * View method
     *
     * @param string|null $id Brand id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $brand = $this->Brands->get($id, [
            'contain' => ['Cities', 'States', 'Users']
        ]);

        $this->set('brand', $brand);
        $this->set('_serialize', ['brand']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $brand = $this->Brands->newEntity();
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->newEntity();
        
        if ($this->request->is('post')) {
            $fileName = $this->request->data['company_icon']['name'];
            $uploadPath = 'uploads/brands/';
            $uploadFile = $uploadPath .'_'.time().$fileName;
            $password=$this->request->data['password'];
            $cpass=$this->request->data['confirm_password'];
            if($password != $cpass){
                 $this->Flash->error(__('Password dose not match. Please, try again.'));
                
            }else if (move_uploaded_file($this->request->data['company_icon']['tmp_name'], $uploadFile)) {

                $brand = $this->Brands->patchEntity($brand, $this->request->getData());
                $user = $usersTable->patchEntity($user, $this->request->getData());
                $brand['company_icon'] = $uploadFile;
                $brand['uuid'] = $this->Uuid->v4();

                $user['uuid'] = $this->Uuid->v4();
                $user['status'] = "ACTIVE";
                $user['user_type'] = "BRAND";
                $user['name'] = $brand['company_name'];
                
                if ($usersTable->save($user)) {
                    // The $article entity contains the id now
                    $brand['user_id'] = $user['id'];
                    if ($this->Brands->save($brand)) {
                        
                        return $this->redirect(['action' => 'index']);
                    }
                    $this->Flash->error(__('The brand could not be saved. Please, try again.'));
                }
                $this->Flash->error(__('The Contact Person could not be saved. Please, try again.'));
            }
        }
        
        $states = $this->Brands->States->find('list', ['order'=>'States.title']);
        //$cities = $this->Brands->Cities->find('list', ['order'=>'Cities.title']);
        $this->set(compact('brand', 'cities', 'states', 'users'));
        $this->set('_serialize', ['brand']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Brand id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $brand = $this->Brands->get($id, [
            'contain' => ['Users']
        ]);
        
       
        if ($this->request->is(['patch', 'post', 'put'])) {
            $brand = $this->Brands->patchEntity($brand, $this->request->getData());
            $request_data= $this->request->getData()['user'];
            $fileName = $this->request->data['file_path']['name'];
            $uploadPath = 'uploads/ads/';
            $uploadFile = $uploadPath .'_'.time().$fileName;
            
            if (!empty($fileName) && move_uploaded_file($this->request->data['file_path']['tmp_name'], $uploadFile)) {
                $brand['company_icon']=$uploadFile;
            }
            $brand->user['contact_no']=$this->request->getData()['user']['contact_no'];
            $brand->user['email']=$this->request->getData()['user']['email'];
            $brand->user['gender']=$this->request->getData()['user']['gender'];
            if($this->request->getData()['user']['password']===$this->request->getData()['user']['confirm_password']
                    && !empty($this->request->getData()['user']['password'])){
                 $brand->user['password']=$this->request->getData()['user']['password'];
            }
            if ($this->Brands->Users->save($brand->user)) {
                
            }
            if ($this->Brands->save($brand)) {
                $this->Flash->success(__('The brand has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The brand could not be saved. Please, try again.'));
        }
        $states = $this->Brands->States->find('list', ['order'=>'States.title']);
        $cities = $this->Brands->Cities->find('list', [
            'conditions' => ['Cities.state_id' => $brand->state_id],
            'limit'=>100,'order'=>'Cities.title']);
        $this->set(compact('brand', 'cities', 'states'));
        $this->set('_serialize', ['brand']);
    }

    /**
     * Delete method
     *       
     * @param string|null $id Brand id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $brand = $this->Brands->get($id);
        if ($this->Brands->delete($brand)) {
            $this->Flash->success(__('The brand has been deleted.'));
        } else {
            $this->Flash->error(__('The brand could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
