<?php

namespace App\View\Helper;

use Cake\View\Helper;

class SideMenuHelper extends Helper {

    function display($data) {
        //This extracting only the name of the menu from the multidimentional array.
        
        $ret = '';
        foreach ($data as $value) {
           
            $ret = '' . $ret . '<li><a href="'.$value['link'].'"><i class="'.$value['icon'].'">
                        <div class="icon-bg bg-orange"></div>
                    </i><span class="menu-title">' . $value['title'] . '</span></a></li>';
        }
        return $ret;
    }

}
