<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DeviceDetail Entity
 *
 * @property int $id
 * @property string $type
 * @property string $device_id
 * @property string $token
 * @property int $userid
 * @property bool $logged_in
 *
 * @property \App\Model\Entity\Device $device
 */
class DeviceDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type' => true,
        'device_id' => true,
        'token' => true,
        'userid' => true,
        'logged_in' => true,
        'mode' => true,
        'device' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'token'
    ];
}
