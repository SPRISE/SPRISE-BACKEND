<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cm Entity
 *
 * @property int $id
 * @property string $about_us
 * @property string $tmc
 */
class Cm extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'about_us' => true,
        'term_and_conditins' => true
    ];
}
