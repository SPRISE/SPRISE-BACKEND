<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Brand Entity
 *
 * @property int $id
 * @property string $uuid
 * @property string $company_name
 * @property string $company_icon
 * @property string $about_company
 * @property int $userid
 * @property string $street_name
 * @property int $city_id
 * @property int $state_id
 * @property string $status
 * @property \Cake\I18n\FrozenTime $create_at
 * @property \Cake\I18n\FrozenTime $modified_at
 *
 * @property \App\Model\Entity\City $city
 * @property \App\Model\Entity\State $state
 */
class Brand extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'uuid' => true,
        'company_name' => true,
        'company_icon' => true,
        'about_company' => true,
        'user_id' => true,
        'street_name' => true,
        'city_id' => true,
        'state_id' => true,
        'status' => true,
        'create_at' => true,
        'modified_at' => true,
        'city' => true,
        'state' => true
    ];
}
