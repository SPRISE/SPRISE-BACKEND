<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * City Entity
 *
 * @property int $id
 * @property string $title
 * @property int $state_id
 *
 * @property \App\Model\Entity\State $state
 * @property \App\Model\Entity\Advertisement[] $advertisements
 * @property \App\Model\Entity\Brand[] $brands
 * @property \App\Model\Entity\User[] $users
 */
class City extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'state_id' => true,
        'state' => true,
        'advertisements' => true,
        'brands' => true,
        'users' => true
    ];
}
