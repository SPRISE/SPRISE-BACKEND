<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $uuid
 * @property string $email
 * @property string $contact_no
 * @property string $password
 * @property string $name
 * @property string $gender
 * @property int $city_id
 * @property int $state_id
 * @property string $status
 * @property \Cake\I18n\FrozenTime $created_at
 * @property \Cake\I18n\FrozenTime $modified_at
 * @property string $user_type
 * @property bool $notification
 *
 * @property \App\Model\Entity\City $city
 * @property \App\Model\Entity\State $state
 * @property \App\Model\Entity\AdViewer[] $ad_viewers
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'uuid' => true,
        'email' => true,
        'contact_no' => true,
        'password' => true,
        'name' => true,
        'gender' => true,
        'city_id' => true,
        'state_id' => true,
        'status' => true,
        'created_at' => true,
        'modified_at' => true,
        'user_type' => true,
        'notification' => true,
        'city' => true,
        'state' => true,
        'ad_viewers' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
    
    protected function _setPassword($password){
       // return (new \Cake\Auth\DefaultPasswordHasher())->hash($password);
        return md5($password);
    }
}
