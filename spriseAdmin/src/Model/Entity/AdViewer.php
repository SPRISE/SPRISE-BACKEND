<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AdViewer Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $ad_id
 * @property \Cake\I18n\FrozenTime $viewed_at
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Advertisement $advertisement
 */
class AdViewer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'ad_id' => true,
        'viewed_at' => true,
        'user' => true,
        'advertisement' => true
    ];
}
