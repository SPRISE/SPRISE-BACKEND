<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Advertisement Entity
 *
 * @property int $id
 * @property string $uuid
 * @property string $title
 * @property string $description
 * @property string $image_url
 * @property string $embded_link
 * @property int $state_id
 * @property int $city_id
 * @property \Cake\I18n\FrozenTime $schedule_date
 * @property string $animation_type
 * @property int $created_by
 * @property string $status
 * @property string $why_rejected
 * @property \Cake\I18n\FrozenTime $created_at
 * @property \Cake\I18n\FrozenTime $modified_at
 *
 * @property \App\Model\Entity\State $state
 * @property \App\Model\Entity\City $city
 */
class Advertisement extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'uuid' => true,
        'title' => true,
        'description' => true,
        'image_url' => true,
        'embded_link' => true,
        'state_id' => true,
        'city_id' => true,
        'schedule_date' => true,
        'animation_type' => true,
        'created_by' => true,
        'status' => true,
        'why_rejected' => true,
        'created_at' => true,
        'modified_at' => true,
        'state' => true,
        'city' => true
    ];
}
