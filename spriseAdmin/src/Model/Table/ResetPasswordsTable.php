<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ResetPasswords Model
 *
 * @property \App\Model\Table\PasswordsTable|\Cake\ORM\Association\BelongsTo $Passwords
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\ResetPassword get($primaryKey, $options = [])
 * @method \App\Model\Entity\ResetPassword newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ResetPassword[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ResetPassword|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ResetPassword patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ResetPassword[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ResetPassword findOrCreate($search, callable $callback = null, $options = [])
 */
class ResetPasswordsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('reset_passwords');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Passwords', [
            'foreignKey' => 'password_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('created_at')
            ->requirePresence('created_at', 'create')
            ->notEmpty('created_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['password_id'], 'Passwords'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
