<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cms Model
 *
 * @method \App\Model\Entity\Cm get($primaryKey, $options = [])
 * @method \App\Model\Entity\Cm newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Cm[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Cm|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cm patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Cm[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Cm findOrCreate($search, callable $callback = null, $options = [])
 */
class CmsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cms');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('about_us')
            ->maxLength('about_us', 16777215)
            ->allowEmpty('about_us');

        $validator
            ->scalar('term_and_conditins')
            ->maxLength('term_and_conditins', 16777215)
            ->allowEmpty('term_and_conditins');

        return $validator;
    }
}
