<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ResetPasswordsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ResetPasswordsController Test Case
 */
class ResetPasswordsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.reset_passwords',
        'app.passwords',
        'app.users',
        'app.cities',
        'app.states',
        'app.advertisements',
        'app.brands',
        'app.ad_viewers'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
