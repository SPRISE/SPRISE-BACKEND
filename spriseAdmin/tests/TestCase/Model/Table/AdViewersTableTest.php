<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AdViewersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AdViewersTable Test Case
 */
class AdViewersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AdViewersTable
     */
    public $AdViewers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ad_viewers',
        'app.users',
        'app.cities',
        'app.states',
        'app.advertisements',
        'app.brands'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AdViewers') ? [] : ['className' => AdViewersTable::class];
        $this->AdViewers = TableRegistry::get('AdViewers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AdViewers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
