<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CmsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CmsTable Test Case
 */
class CmsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CmsTable
     */
    public $Cms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cms'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Cms') ? [] : ['className' => CmsTable::class];
        $this->Cms = TableRegistry::get('Cms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Cms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
