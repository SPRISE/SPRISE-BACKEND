<?php

/* Require Slim and plugins */
include 'UUID.php';
require 'Slim/Slim.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';


//require 'plugins/NotORM.php';
//date_default_timezone_set('Asia/Calcutta');
/* Register autoloader and instantiate Slim */
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$BASE_PATH = "http://surprise.hockeysticktech.com/spriseAdmin/";
/* Db Connection */

$user_id = NULL;

function getDB() {
    //$dbhost = "192.168.1.250";
    $dbhost = "localhost";
    $dbuser = "usr_surprise";
    $dbpass = "d9k5PR4rz02G";
    $dbname = "db_surprise";

    $mysql_conn_string = "mysql:host=$dbhost;dbname=$dbname";
    $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );
    $dbConnection = new PDO($mysql_conn_string, $dbuser, $dbpass, $options);
    $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbConnection;
}

/* Default api */
$app->get('/', function() use($app) {
    $app->response->setStatus(200);
    echo "Welcome to SPrise Webservices : ";
    print_r(date("Y-m-d H:i:s"));
});

function parameterListIsExistWithValue($parameters, $allPostVars) {
    $app = \Slim\Slim::getInstance();
    if ($allPostVars == null) {
        $app->response->setStatus(200);
        $app->response()->headers->set('Content-Type', 'application/json');
        echo json_encode(array("responseCode" => 0, "message" => "Parameters Missing"));
        return false;
    }
    for ($i = 0; $i < count($parameters); $i++) {
        if (!array_key_exists($parameters[$i], $allPostVars)) {
            $app->response->setStatus(200);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 0, "message" => "Parameter '" . $parameters[$i] . "' is missing"));
            return false;
        }
        if (trim($allPostVars[$parameters[$i]]) == '') {
            $app->response->setStatus(200);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 0, "message" => "Parameter '" . $parameters[$i] . "' should not be empty."));
            return false;
        }
    }
    return true;
}

function authenticate(\Slim\Route $route) {
    $app = \Slim\Slim::getInstance();
    $realm = 'Protected APIS';
    $req = $app->request();
    $res = $app->response();

    $username = $req->headers('PHP_AUTH_USER');
    $password = $req->headers('PHP_AUTH_PW');

    if (isset($username) && $username != '' && isset($password) && $password != '') {
        if ($userdata = validateUser($username, $password)) {
            global $user_id;
            $user_id = $userdata;
            return true;
        } else {
            $app->response->header('WWW-Authenticate', sprintf('Basic realm="%s"', $realm));
            $app->response->setStatus(401);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 0, "message" => "Authontication Failerar"));
            $app->stop();
        }
    } else {
        $app->response->header('WWW-Authenticate', sprintf('Basic realm="%s"', $realm));
        $app->response->setStatus(401);
        $app->response()->headers->set('Content-Type', 'application/json');
        echo json_encode(array("responseCode" => 0, "message" => "Authontication Failerar"));
        $app->stop();
    }
}

function validateUser($email, $password) {
    $password = md5($password);
    $db = getDB();
    $selectUser = $db->prepare("SELECT id from users WHERE email = ? AND password = ? and status='ACTIVE'");
    $selectUser->execute(array($email, $password));
    if ($selectUser->rowCount() > 0) {
        $rs = $selectUser->fetchAll(PDO::FETCH_ASSOC);
        $userid = $rs[0]['id'];
        return $userid;
    }
}

/* Register */
$app->post('/Register', function() use($app) {
    $app = \Slim\Slim::getInstance();
    $json = $app->request->getBody();
    $allPostVars = json_decode($json, true);

    if (!parameterListIsExistWithValue(array('name', 'email', 'password', 'cityId', 'stateId', 'deviceType', 'deviceId', 'deviceToken'), $allPostVars)) {
        exit;
    }

    $name = $allPostVars['name'];
    $email = $allPostVars['email'];
    $contactNo = "";
    if (isset($allPostVars['contactNo']))
        $contactNo = $allPostVars['contactNo'];

    $password = md5($allPostVars['password']);
    $cityId = $allPostVars['cityId'];
    $stateId = $allPostVars['stateId'];
    $deviceType = $allPostVars['deviceType'];
    $deviceId = $allPostVars['deviceId'];
    $deviceToken = $allPostVars['deviceToken'];

    $gender = null;
    if (isset($allPostVars['gender']))
        $gender = $allPostVars['gender'];

    if (!($deviceType == 'A' || $deviceType == 'I')) {
        $app->response->setStatus(201);
        $app->response()->headers->set('Content-Type', 'application/json');
        echo json_encode(array("responseCode" => 4, "message" => "Invalid deviceType provided"));
        exit;
    }

    if ($gender != null && !($gender == 'M' || $gender == 'F')) {
        $app->response->setStatus(201);
        $app->response()->headers->set('Content-Type', 'application/json');
        echo json_encode(array("responseCode" => 0, "message" => "Invalid gender provided"));
        exit;
    }

    try {
        $db = getDB();
        $selectCity = $db->prepare("SELECT `id`, `title`, `state_id` FROM `cities` WHERE `state_id` = ? AND `id` = ?");
        $selectCity->execute(array($stateId, $cityId));
        $rowCount = $selectCity->rowCount();
        if ($rowCount == 0) {
            $app->response->setStatus(200);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 0, "message" => "Invaid city and state provided."));
            $db = null;
            exit;
        }

        $chkEmailExist = $db->prepare("SELECT `users`.`email` from `users` WHERE `users`.`email`=?");
        $chkEmailExist->execute(array($email));
        if ($chkEmailExist->rowCount() > 0) {
            $app->response->setStatus(200);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 2, "message" => "User with the given email already registerd."));
            $db = null;
            exit;
        }

        $todayDate = date("Y-m-d H:i:s");

        $userUUID = UUID::v4();
        $addUser = $db->prepare("INSERT INTO `users`(`uuid`, `email`, `contact_no`, `password`, `name`, `gender`, `city_id`, `state_id`, `status`, `created_at`, `modified_at`, `user_type`, `notification`) 
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $addUser->execute(array($userUUID, $email, $contactNo, $password, $name, $gender, $cityId, $stateId, 'ACTIVE', $todayDate, $todayDate, "USER", true));
        if ($addUser->rowCount() > 0) {
            $userId = $db->lastInsertId();

            $addDevice = $db->prepare("INSERT INTO `device_details`(`token`, `type`, `device_id`, `user_id`, `logged_in`) VALUES(?, ?, ?, ?, ?)");
            $addDevice->execute(array($deviceToken, $deviceType, $deviceId, $userId, true));
            if ($addDevice->rowCount() > 0) {
                $deviceId = $db->lastInsertId();
            }

            $userDetail = getUserDetailByUserId($db, $userId);
            $db = null;
            $app->response->setStatus(200);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 1, "message" => "User Registration Successfully Completed", "userDetail" => $userDetail));
            $db = null;
            exit;
        } else {
            $app->response->setStatus(201);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 0, "message" => "Unable to register user right now."));
            $db = null;
            exit;
        }
    } catch (PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
});
/* End */


/* Login */
$app->post('/Login', function() use($app) {
    $app = \Slim\Slim::getInstance();
    $json = $app->request->getBody();
    $allPostVars = json_decode($json, true);

    if (!parameterListIsExistWithValue(array('email', 'password', 'deviceType', 'deviceId', 'deviceToken'), $allPostVars)) {
        exit;
    }

    $email = $allPostVars['email'];
    $password = md5($allPostVars['password']);
    $deviceType = $allPostVars['deviceType'];
    $deviceId = $allPostVars['deviceId'];
    $deviceToken = $allPostVars['deviceToken'];

    if (!($deviceType == 'A' || $deviceType == 'I')) {
        $app->response->setStatus(201);
        $app->response()->headers->set('Content-Type', 'application/json');
        echo json_encode(array("responseCode" => 4, "message" => "Invalid deviceType provided"));
        exit;
    }

    try {
        $db = getDB();

        $chkUserLogin = $db->prepare("SELECT `id`, `uuid`, `email`, `contact_no`, `password`, `name`, `gender`, `city_id`, `state_id`, `status`, `notification` FROM `users` WHERE `email`=? AND `user_type`=? LIMIT 0, 1");
        $chkUserLogin->execute(array($email, 'USER'));
        if ($chkUserLogin->rowCount() > 0) {
            $rs = $chkUserLogin->fetchAll(PDO::FETCH_ASSOC);

            if ($rs[0]["password"] != $password) {
                $app->response->setStatus(201);
                $app->response()->headers->set('Content-Type', 'application/json');
                echo json_encode(array("responseCode" => 8, "message" => "Invalid login details."));
                exit;
            }

            if ($rs[0]["status"] != "ACTIVE") {
                $app->response->setStatus(201);
                $app->response()->headers->set('Content-Type', 'application/json');
                echo json_encode(array("responseCode" => 0, "message" => "User is Deactivated, please contact to your admin."));
                exit;
            }

            $userId = $rs[0]["id"];
            $selectDevice = $db->prepare("SELECT `id` FROM `device_details` WHERE `device_id` = ? AND `user_id` = ?");
            $selectDevice->execute(array($deviceId, $userId));
            if ($selectDevice->rowCount() > 0) {
                $selectDeviceRS = $selectDevice->fetchAll(PDO::FETCH_ASSOC);
                $updateDevice = $db->prepare("UPDATE `device_details` SET `logged_in` = ?, `token` = ?, `type` = ? WHERE `id` = ?;");
                $updateDevice->execute(array(1, $deviceToken, $deviceType, $selectDeviceRS[0]['id']));
            } else {
                $addDevice = $db->prepare("INSERT INTO `device_details`(`token`, `type`, `device_id`, `user_id`, `logged_in`) VALUES(?, ?, ?, ?, ?)");
                $addDevice->execute(array($deviceToken, $deviceType, $deviceId, $userId, 1));
            }

            $userDetail = getUserDetailByUserId($db, $userId);

            $db = null;
            $app->response->setStatus(200);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 1, "message" => "User Login Successful.", "userDetail" => $userDetail));
            exit;
        } else {
            $app->response->setStatus(201);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 8, "message" => "Invalid login details."));
            exit;
        }
    } catch (PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
});
/* End */


/* Logout */
$app->post('/Logout', 'authenticate', function() use($app) {
    $app = \Slim\Slim::getInstance();
    $json = $app->request->getBody();
    $allPostVars = json_decode($json, true);
    if (!parameterListIsExistWithValue(array('deviceId'), $allPostVars)) {
        exit;
    }
    $deviceId = $allPostVars['deviceId'];

    global $user_id;
    try {
        $db = getDB();

        $selectDevice = $db->prepare("SELECT `id` FROM `device_details` WHERE `device_id` = ? AND `user_id` = ?");
        $selectDevice->execute(array($deviceId, $user_id));

        if ($selectDevice->rowCount() > 0) {
            $selectDeviceRS = $selectDevice->fetchAll(PDO::FETCH_ASSOC);
            $updateDevice = $db->prepare("UPDATE `device_details` SET `logged_in` = ? WHERE `id` = ?;");
            $updateDevice->execute(array(0, $selectDeviceRS[0]['id']));
        }

        $db = null;
        $app->response->setStatus(201);
        $app->response()->headers->set('Content-Type', 'application/json');
        echo json_encode(array("responseCode" => 1, "message" => "User Logout Successfully."));
        exit;
    } catch (PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
});
/* End */


/* DeviceDetailUpdate */
$app->post('/DeviceDetailUpdate', 'authenticate', function() use($app) {
    $app = \Slim\Slim::getInstance();
    $json = $app->request->getBody();
    $allPostVars = json_decode($json, true);
    global $user_id;
    if (!parameterListIsExistWithValue(array('deviceType', 'deviceToken', 'deviceId'), $allPostVars)) {
        exit;
    }

    $deviceType = $allPostVars['deviceType'];
    $deviceToken = $allPostVars['deviceToken'];
    $deviceId = $allPostVars['deviceId'];

    if (!($deviceType == 'A' || $deviceType == 'I')) {
        $app->response->setStatus(201);
        $app->response()->headers->set('Content-Type', 'application/json');
        echo json_encode(array("responseCode" => 4, "message" => "Invalid deviceType provided"));
        exit;
    }

    try {
        $db = getDB();

        $selectDevice = $db->prepare("SELECT `id` FROM `device_details` WHERE `device_id` = ? AND `type` = ? AND `user_id` = ?");
        $selectDevice->execute(array($deviceId, $deviceType, $user_id));
        if ($selectDevice->rowCount() > 0) {
            $selectDeviceRS = $selectDevice->fetchAll(PDO::FETCH_ASSOC);
            $updateDevice = $db->prepare("UPDATE `device_details` SET `token` = ?, `type` = ? WHERE `id` = ?;");
            $updateDevice->execute(array($deviceToken, $deviceType, $selectDeviceRS[0]['id']));
        } else {
            $addDevice = $db->prepare("INSERT INTO `device_details`(`token`, `type`, `device_id`, `user_id`, `logged_in`) VALUES(?, ?, ?, ?, ?)");
            $addDevice->execute(array($deviceToken, $deviceType, $deviceId, $user_id, 1));
        }

        $userDetail = getUserDetailByUserId($db, $user_id);

        $db = null;
        $app->response->setStatus(200);
        $app->response()->headers->set('Content-Type', 'application/json');
        echo json_encode(array("responseCode" => 1, "message" => "Device detail updated successfully", "userDetail" => $userDetail));
        exit;
    } catch (PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
});
/* End */


/* UpdateNotification */
$app->post('/UpdateNotification', 'authenticate', function() use($app) {
    $app = \Slim\Slim::getInstance();
    $json = $app->request->getBody();
    $allPostVars = json_decode($json, true);
    global $user_id;
    if (!parameterListIsExistWithValue(array('notificationEnable'), $allPostVars)) {
        exit;
    }

    $notificationEnable = $allPostVars['notificationEnable'];

    try {
        $db = getDB();
        $updateUser = $db->prepare("UPDATE `users` SET `notification` = ? WHERE `id` = ?;");
        $updateUser->execute(array($notificationEnable, $user_id));
        $db = null;
        $app->response->setStatus(200);
        $app->response()->headers->set('Content-Type', 'application/json');
        echo json_encode(array("responseCode" => 1, "message" => "Notifiation setting updated successfully"));
        exit;
    } catch (PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
});
/* End */


/* StateList */
$app->get('/StateList', function() use($app) {
    $app = \Slim\Slim::getInstance();

    try {
        $db = getDB();
        $selectState = $db->prepare("SELECT `id`, `title` FROM `states` Order by `title`;");
        $selectState->execute();
        $rowCount = $selectState->rowCount();
        if ($rowCount > 0) {
            $rs = $selectState->fetchAll(PDO::FETCH_ASSOC);
            $stateArray = array();
            for ($i = 0; $i < $rowCount; $i++) {
                $stateObject = array(
                    "stateId" => $rs[$i]["id"],
                    "stateTitle" => $rs[$i]["title"]
                );
                $stateArray[] = $stateObject;
            }
            $app->response->setStatus(200);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 1, "message" => "State list", "stateList" => $stateArray));
        } else {
            $app->response->setStatus(200);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 0, "message" => "No State found"));
        }
        $db = null;
        exit;
    } catch (PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
});
/* End */


/* CityList */
$app->get('/CityList/:stateId', function($stateId) use($app) {
    $app = \Slim\Slim::getInstance();
    try {
        $db = getDB();
        $selectCity = $db->prepare("SELECT `id`, `title` FROM `cities` WHERE `state_id` = ? order by `title`;");
        $selectCity->execute(array($stateId));
        $rowCount = $selectCity->rowCount();
        if ($rowCount > 0) {
            $rs = $selectCity->fetchAll(PDO::FETCH_ASSOC);
            $cityArray = array();
            for ($i = 0; $i < $rowCount; $i++) {
                $cityObject = array(
                    "cityId" => $rs[$i]["id"],
                    "cityTitle" => $rs[$i]["title"]
                );
                $cityArray[] = $cityObject;
            }
            $app->response->setStatus(200);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 1, "message" => "City list", "cityList" => $cityArray));
        } else {
            $app->response->setStatus(200);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 0, "message" => "No City found in that state."));
        }
        $db = null;
        exit;
    } catch (PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
});
/* End */


/* AdsList */
$app->post('/AdsList', 'authenticate', function() use($app) {
    $app = \Slim\Slim::getInstance();
    $json = $app->request->getBody();
    $allPostVars = json_decode($json, true);

    if (!parameterListIsExistWithValue(array('startCount', 'pageSize', 'stateId', 'cityId'), $allPostVars)) {
        exit;
    }

    $startCount = $allPostVars['startCount'];
    $pageSize = $allPostVars['pageSize'];
    $stateId = $allPostVars['stateId'];
    $cityId = $allPostVars['cityId'];

    $mode = null;
    if (isset($allPostVars['mode']))
        $mode = $allPostVars['mode'];
    $deviceId = null;
    if (isset($allPostVars['deviceId']))
        $deviceId = $allPostVars['deviceId'];

    global $user_id;
    try {
        $db = getDB();

        if ($mode != null) {
            $updateDevice = $db->prepare("UPDATE `device_details` SET `mode` = ? WHERE `user_id` = ? AND `device_id` = ?;");
            $updateDevice->execute(array($mode, $user_id, $deviceId));
        }

        $selectAds = $db->prepare("SELECT 
                                            `advertisements`.`uuid`, 
                                            `advertisements`.`title`, 
                                            `advertisements`.`image_url`, 
                                            `advertisements`.`description`, 
                                            `advertisements`.`animation_type`, 
                                            `advertisements`.`schedule_date`, 
                                            `advertisements`.`embded_link`,
                                            `ad_viewers`.`user_id` AS seen,
                                            `brands`.`company_name` AS brandName,
                                            `brands`.`company_icon` AS brandImage
                                    FROM 
                                            `advertisements` 
                                                    LEFT JOIN `ad_viewers` ON `advertisements`.`id` = `ad_viewers`.`ad_id` AND `ad_viewers`.`user_id` = ?
                                                    LEFT JOIN `brands` ON `advertisements`.`created_by` = `brands`.`user_id`

                                    WHERE 
                                            `advertisements`.`state_id` = ? AND 
                                            `advertisements`.`city_id` = ? AND 
                                            `advertisements`.`status` = 'ACTIVE' AND 
                                            `advertisements`.`schedule_date` <= NOW() 
                                    ORDER BY
                                            `advertisements`.`schedule_date` desc
                                    LIMIT $startCount, $pageSize;");


        $selectAds->execute(array($user_id, $stateId, $cityId));
        $rowCount = $selectAds->rowCount();
        global $BASE_PATH;
        if ($rowCount > 0) {
            $rs = $selectAds->fetchAll(PDO::FETCH_ASSOC);
            $adsArray = array();
            for ($i = 0; $i < $rowCount; $i++) {
                $AdObject = array(
                    "adId" => $rs[$i]["uuid"],
                    "adTitle" => $rs[$i]["title"],
                    "imageURL" => $BASE_PATH . $rs[$i]["image_url"],
                    "embdedLink" => $rs[$i]["embded_link"],
                    "description" => $rs[$i]["description"],
                    "animationType" => $rs[$i]["animation_type"],
                    "brandName" => $rs[$i]["brandName"],
                    "brandImage" => $BASE_PATH . $rs[$i]["brandImage"],
                    "scheduleDate" => formatDate($rs[$i]["schedule_date"]),
                    "seen" => ($rs[$i]["seen"] != null ? true : false)
                );
                $adsArray[] = $AdObject;
            }
            $app->response->setStatus(200);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 1, "message" => "Ad List", "adList" => $adsArray));
        } else {
            $app->response->setStatus(200);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 0, "message" => "No Ads found in your city"));
        }
        $db = null;
        exit;
    } catch (PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
});
/* End */


/* AdsList */
$app->get('/AdDetail/:adId', 'authenticate', function($adId) use($app) {
    $app = \Slim\Slim::getInstance();
    $json = $app->request->getBody();
    $allPostVars = json_decode($json, true);
    global $user_id;
    try {
        $db = getDB();
        $selectAds = $db->prepare("SELECT `id`, `uuid`, `title`, `description`, `image_url`, `embded_link`, `state_id`, `city_id`, `status`, `schedule_date`, `why_rejected` FROM `advertisements` WHERE `uuid` = ?;");
        $selectAds->execute(array($adId));
        $rowCount = $selectAds->rowCount();
        if ($rowCount > 0) {
            $rs = $selectAds->fetchAll(PDO::FETCH_ASSOC);
            $adObject = array(
                "adId" => $rs[0]["uuid"],
                "adTitle" => $rs[0]["title"],
                "description" => $rs[0]["description"],
                "imageURL" => $rs[0]["image_url"],
                "embdedLink" => $rs[0]["embded_link"],
                "stateId" => $rs[0]["state_id"],
                "cityId" => $rs[0]["city_id"],
                "status" => $rs[0]["status"],
                "scheduleDate" => formatDate($rs[0]["schedule_date"])
            );

            $selectAdViewer = $db->prepare("SELECT `id` FROM `ad_viewers` WHERE `user_id` = ? AND `ad_id` = ?;");
            $selectAdViewer->execute(array($user_id, $rs[0]["id"]));
            if ($selectAdViewer->rowCount() == 0) {
                $todayDate = date("Y-m-d H:i:s");
                $adViewer = $db->prepare("INSERT INTO `ad_viewers` (`user_id`, `ad_id`, `viewed_at`) VALUES (?, ?, ?);");
                $adViewer->execute(array($user_id, $rs[0]["id"], $todayDate));
            }
            $app->response->setStatus(200);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 1, "message" => "Ad Detail", "adList" => $adObject));
        } else {
            $app->response->setStatus(200);
            $app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode(array("responseCode" => 0, "message" => "No Ads found with given id"));
        }
        $db = null;
        exit;
    } catch (PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
});
/* End */


/* UpdateProfile */
$app->post('/UpdateProfile', 'authenticate', function() use($app) {
    $app = \Slim\Slim::getInstance();
    $json = $app->request->getBody();
    $allPostVars = json_decode($json, true);

    if (!parameterListIsExistWithValue(array('name', 'cityId', 'stateId'), $allPostVars)) {
        exit;
    }

    $name = $allPostVars['name'];
    $cityId = $allPostVars['cityId'];
    $stateId = $allPostVars['stateId'];

    global $user_id;
    try {
        $db = getDB();

        $todayDate = date("Y-m-d H:i:s");
        $udpteUser = $db->prepare("UPDATE `users` SET `name` = ?, `city_id` = ?, `state_id` = ?, `modified_at` = ? WHERE `id` = ?;");
        $udpteUser->execute(array($name, $cityId, $stateId, $todayDate, $user_id));
        $rowCount = $udpteUser->rowCount();
        $app->response->setStatus(200);
        $app->response()->headers->set('Content-Type', 'application/json');
        if ($rowCount > 0) {
            $userDetail = getUserDetailByUserId($db, $user_id);
            echo json_encode(array("responseCode" => 1, "message" => "User updated successfully", "userDetail" => $userDetail));
        } else {
            echo json_encode(array("responseCode" => 0, "message" => "Unable to update user"));
        }
        $db = null;
        exit;
    } catch (PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
});
/* End */


/* TermAndConditins */
$app->get('/TermAndConditins', function() use($app) {
    $app = \Slim\Slim::getInstance();
    try {
        $db = getDB();
        $selectTNC = $db->prepare("SELECT `term_and_conditins` FROM `cms` where `id` = ?");
        $selectTNC->execute(array(1));
        $rowCount = $selectTNC->rowCount();

        $app->response->setStatus(200);
        $app->response()->headers->set('Content-Type', 'application/json');

        if ($rowCount > 0) {
            $rs = $selectTNC->fetchAll(PDO::FETCH_ASSOC);
            $termAndConditins = $rs[0]["term_and_conditins"];
            echo json_encode(array("responseCode" => 1, "message" => "Term And Conditins", "termAndConditins" => $termAndConditins));
        } else {
            echo json_encode(array("responseCode" => 0, "message" => "No Data Found"));
        }
        $db = null;
        exit;
    } catch (PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
});
/* End */


/* ForGotPassword */
$app->post('/ForGotPassword', function() use($app) {
    $app = \Slim\Slim::getInstance();
    $json = $app->request->getBody();
    $allPostVars = json_decode($json, true);

    $uuid = UUID::v4();
    $uuid .= "-";
    $uuid .= UUID::v4();

    if (!parameterListIsExistWithValue(array('email'), $allPostVars)) {
        exit;
    }
    $email = $allPostVars['email'];

    try {
        $db = getDB();

        $selectUser = $db->prepare("SELECT `id`, `email`, `name` FROM `users` WHERE `email` = ?;");
        $selectUser->execute(array($email));
        $rowCount = $selectUser->rowCount();

        $app->response->setStatus(200);
        $app->response()->headers->set('Content-Type', 'application/json');
        if ($rowCount > 0) {
            $rs = $selectUser->fetchAll(PDO::FETCH_ASSOC);
            $userId = $rs[0]["id"];
            $userEmail = $rs[0]["email"];
            $userName = $rs[0]["name"];

            $addResetPasswordLink = $db->prepare("INSERT INTO `reset_passwords` (`password_id`, `user_id`) VALUES (?, ?);");
            $addResetPasswordLink->execute(array($uuid, $userId));
            global $BASE_PATH;
            $finalURL = $BASE_PATH . "ResetPasswords?pid=$uuid";

            $htmlBody = "<body style='background: #f3f3f3; margin: 0 auto;'>
							<div style='background: #f3f3f3; width: 100%; height: 100vh; margin: 0 auto; padding: 0; font-family: sans-serif;'>
								<div style='max-width: 650px; width:100%; margin: 0 auto;'>
									<table width='100%' style='background: linear-gradient(#6de8fc,#3560dd); color: #FFF;'>
										<tr>
											<td align='center' style='padding:30px 0 15px 0;'>
												<img src='http://surprise.hockeysticktech.com/sprise/sprise_logo.png' width='200' alt='Timewise'>
											</td>
										</tr>
										<tr>
											<td align='center' style='padding:15px 0; font-weight: bold; font-size: 18px;'>
												Forgot your password?
											</td>
										</tr>
										<tr>
											<td align='center' style='padding:0 0 30px 0;'>
												Calm, you'll get a new one in a moment!
											</td>
										</tr>
									</table>
									<table width='100%' style='background: #FFF; padding: 30px 50px; text-align: center;'>
										<tr>
											<td style='padding: 15px 0; font-weight: bold;'>
												Hello $userName,
											</td>
										</tr>
										<tr>
											<td style='padding: 15px 0; font-weight: bold;'>
												Follow the link below to reset your password!
											</td>
										</tr>
										<tr>
											<td style='padding: 15px 0; font-weight: bold;'>
												<a href='$finalURL' style='border:none; background:#1976d3; color:#FFF; padding:10px 15px; border-radius:5px; text-decoration:none; border-bottom:3px solid #0f569e;'>Reset Password</a>
											</td>
										</tr>
										<tr>
											<td style='padding: 15px 0; font-size: 12px;'>
												If you do not request a new password, you can ignore this message
											</td>
										</tr>
									</table>
									<p style='font-size: 12px; padding: 0 0 15px 0; text-align: center;'>
										Copywright 2017 S'prise
									</p>
								</div>
							</div>
						</body>";
            $message = smtpMailer($userEmail, $userName, "Change Password Link", $htmlBody);

            echo json_encode(array("responseCode" => 1,
                "message" => "We have send you reset password link on you email, Please check your email. ",
                "mailStatus" => $message,
                "URL" => $finalURL
            ));
        } else {
            echo json_encode(array("responseCode" => 0, "message" => "User not found with the given email address."));
        }
        $db = null;
        exit;
    } catch (PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
});
/* End */


/* Notification */
/*$app->get('/FCMNotification/:devicdToken', function($devicdToken) use($app) {
    $app = \Slim\Slim::getInstance();
    try {
        $message = "Test Message";
        $result = pushNotification($devicdToken, $message, "http://thegadgetsquare.com/wp-content/uploads/2017/07/images-7-2.jpg");
        echo("REsutl : ");
        print_r($result);
        exit;
    } catch (PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
});
/* End */


/* Notification */
$app->get('/APNSNotification/:devicdToken', function($devicdToken) use($app) {
    $app = \Slim\Slim::getInstance();
    try {
        $message = "Test Message";
        $result = pushNotificationIOS($devicdToken, $message);
        echo("REsutl : ");
        print_r($result);
        exit;
    } catch (PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
});
/* End */

function getUserDetailByUserId($db, $userId) {
    $userDetail = $db->prepare("SELECT 
									`users`.`id`, `users`.`uuid`, `users`.`email`, `users`.`contact_no`, `users`.`password`, `users`.`name`, `users`.`gender`, 
									`users`.`city_id`, `cities`.`title` AS 'cityTitle',
									`users`.`state_id`, `states`.`title` AS 'stateTitle',
									`users`.`status`, `users`.`notification` 
								FROM 
									`users` 
										LEFT JOIN `cities` ON `cities`.`id` = `users`.`city_id`
										LEFT JOIN `states` ON `states`.`id` = `users`.`state_id`
								WHERE 
									`users`.`id`=? AND 
									`users`.`user_type`=? 
								LIMIT 0, 1");
    //$userDetail = $db->prepare("SELECT `id`, `uuid`, `email`, `contact_no`, `password`, `name`, `gender`, `city_id`, `state_id`, `status`, `notification` FROM `users` WHERE `id`=? AND `user_type`=? LIMIT 0, 1");
    $userDetail->execute(array($userId, 'USER'));
    if ($userDetail->rowCount() > 0) {
        $rs = $userDetail->fetchAll(PDO::FETCH_ASSOC);
        $userDetail = array(
            "userId" => $rs[0]["uuid"],
            "name" => $rs[0]["name"],
            "email" => $rs[0]["email"],
            "contactNo" => $rs[0]["contact_no"],
            "cityId" => $rs[0]["city_id"],
            "cityTitle" => $rs[0]["cityTitle"],
            "stateId" => $rs[0]["state_id"],
            "stateTitle" => $rs[0]["stateTitle"],
            "gender" => $rs[0]["gender"],
            "notification" => $rs[0]["notification"]
        );
    }
    return $userDetail;
}

// https://www.sitepoint.com/sending-emails-php-phpmailer/
function smtpMailer($to, $toName, $subject, $htmlBody) {
    //PHPMailer Object
    $mail = new PHPMailer;
    //From email address and name
    $mail->From = "from@yourdomain.com";
    $mail->FromName = "Full Name";
    //To address and name
    $mail->addAddress($to, $toName);
    //Address to which recipient will reply
    // $mail->addReplyTo("shashibaheti04@gmail.com", "Reply");
    //Send HTML or Plain Text email
    $mail->isHTML(true);

    $mail->Subject = $subject;
    $mail->Body = $htmlBody;
    $mail->AltBody = "";
    if (!$mail->send()) {
        return "Mailer Error: " . $mail->ErrorInfo;
    } else {
        return "Message has been sent successfully";
    }
}

$app->run();

function pushNotification($deviceIds, $message, $iconImage = "") {
    $api_key = 'AAAAGsx6qOA:APA91bH229xhiUr865PMV6PHqvIliYj3XVPNYan3fuUSKmOghk-7bL4swqfVn7u7lVvJ0dY9L370zFX84KTA3fesgYKUGp-XWWOqfc6pajexf95CziJASFs5NfI5hsY7HpVzC-sl025a'; //get the api key from FCM backend
    $url = 'https://fcm.googleapis.com/fcm/send';
    //$fields = array('registration_ids'  => array($deviceIds));//get the device token from Android 

    $fields = array(
        'to' => $deviceIds,
        'data' => array(
            "message" => $message,
            "title" => "S'Prise",
            "image" => "",
            "iconImage" => $iconImage,
            "timeStamp" => mktime()
        )
    );
    $headers = array(
        'Authorization: key=' . $api_key,
        'Content-Type: application/json'
    );

    // $headers = array( 'Authorization: key=' . $api_key,'Content-Type: application/json');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        return 'Curl error: ' . curl_error($ch);
    }
    curl_close($ch);
    $cur_message = json_decode($result);
    return $cur_message;
}

function pushNotificationIOS($deviceToken, $message) {
    $ctx = stream_context_create();
    //$ckpath = SITEURL.'pushcert.pem';  
    stream_context_set_option($ctx, 'ssl', 'local_cert', 'pem/sprise_dev.pem');
    // stream_context_set_option($ctx, 'ssl', 'local_cert', 'pem/sprise_production.pem');
    // Put your private key's passphrase here: 
    $passphrase = '';
    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
    $fpurl = 'ssl://gateway.sandbox.push.apple.com:2195';

    $body['aps'] = array(
        'alert' => $message,
        'sound' => 'default',
        'badge' => 0,
        'data' => $message,
    );

    $payload = json_encode($body);
    //print_r($payload);exit;
    try {
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
    } catch (Exception $e) {
        return 'FAILURE';
    }
    $fp = null;
    $fp = @stream_socket_client($fpurl, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
    if (!$fp)
        exit("Failed to connect: $err $errstr" . PHP_EOL);

    // Send it to the server
    try {
        $result = fwrite($fp, $msg, strlen($msg));
        fclose($fp);
        $fp = null;
        $ctx = null;
    } catch (Exception $e) {
        if (!$fp) {
            fclose($fp);
        }
        $fp = null;
        $ctx = null;
        return 'FAILURE';
    }
    //print_r($result);
    if ($result)
        return 'SUCCESS';
    else
        return 'FAILURE';
}

function formatDate($dateString) {
    return date("d, M Y", strtotime($dateString));
}

?>